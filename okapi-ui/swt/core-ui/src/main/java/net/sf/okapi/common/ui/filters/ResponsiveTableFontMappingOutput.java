/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.common.ui.filters;

import net.sf.okapi.common.filters.fontmappings.FontMapping;
import net.sf.okapi.common.ui.ResponsiveTable;

import java.util.regex.Pattern;

final class ResponsiveTableFontMappingOutput implements FontMapping.Output<ResponsiveTable> {
    private final ResponsiveTable responsiveTable;

    ResponsiveTableFontMappingOutput(final ResponsiveTable responsiveTable) {
        this.responsiveTable = responsiveTable;
    }

    @Override
    public ResponsiveTable writtenWith(final Pattern sourceLanguagePattern, final Pattern targetLanguagePattern, final Pattern sourceFontPattern, final String targetFont) {
        this.responsiveTable.addRow(
            new String[] {
                sourceLanguagePattern.toString(),
                targetLanguagePattern.toString(),
                sourceFontPattern.toString(),
                targetFont
            }
        );
        return this.responsiveTable;
    }
}

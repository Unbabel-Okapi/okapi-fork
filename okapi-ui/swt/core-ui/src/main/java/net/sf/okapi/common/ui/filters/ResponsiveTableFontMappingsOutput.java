/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.common.ui.filters;

import net.sf.okapi.common.filters.fontmappings.FontMapping;
import net.sf.okapi.common.filters.fontmappings.FontMappings;
import net.sf.okapi.common.ui.ResponsiveTable;

import java.util.Iterator;
import java.util.ResourceBundle;

public final class ResponsiveTableFontMappingsOutput implements FontMappings.Output<ResponsiveTable> {
    private final ResponsiveTable responsiveTable;

    public ResponsiveTableFontMappingsOutput(final ResponsiveTable responsiveTable) {
        this.responsiveTable = responsiveTable;
    }

    @Override
    public ResponsiveTable writtenWith(final Iterator<FontMapping> fontMappingsIterator) {
        final ResourceBundle rb = ResourceBundle.getBundle("net.sf.okapi.common.ui.filters.ResponsiveTableFontMappingsOutput");
        this.responsiveTable.configureHeader(
            new String[] {
                rb.getString("source-locale-pattern"),
                rb.getString("target-locale-pattern"),
                rb.getString("source-font-pattern"),
                rb.getString("target-font")
            }
        );
        while (fontMappingsIterator.hasNext()) {
            fontMappingsIterator.next().writtenTo(
                new ResponsiveTableFontMappingOutput(this.responsiveTable)
            );
        }
        this.responsiveTable.configureBody();
        return this.responsiveTable;
    }
}

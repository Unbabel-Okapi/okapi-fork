# Changes from M30 to M31

<!-- MACRO{toc} -->

## Filters

* General

    * Fixed [issue #519](https://bitbucket.org/okapiframework/okapi/issues/519): When using the subfilter feature in several
      filters (including JSON, YAML, ITS, and XML Stream), it is no longer
      possible to produce multiple text units with the same ID.

* XLIFF Filter

    * Fixed case of memoQ XLIFF files using `<source>`
      and `<target>` elements in its own extensions.
      This resolves [issue #547](https://bitbucket.org/okapiframework/okapi/issues/547).

* JSON Filter

    * The "Use Full Key Path" option will no longer include the prefix
      `"null/"` in all generated resource names. Instead, key paths will
      begin with `"/"`, as in `"/foo/bar"`.

* Properties Filter

    * Added an option to use the key value as the text-unit ID value.
      This addresses [issue #520](https://bitbucket.org/okapiframework/okapi/issues/520).

* OpenXML Filter

    * Fixed a case in which some hidden spreadsheet cells could still be
      exposed for translation.
    * Fixed cases where revision metadata unrelated to translatable text
      could prevent filtering if the "Automatically Accept Revisions"
      option was not checked.
    * If the "Treat Tab as Character" option is enabled, the `'\t'`
      character will now be replaced with a `<tab>` element on output.
    * Added the `tsComplexFieldDefinitionsToExtract` parameter,
      which allows users to specify which field codes should be
      translated. By default, only `HYPERLINK` codes are translated,
      matching the existing behavior.

* XML Filter

    * In the RESX pre-defined configuration: fixed the code-finder
      expression for mustache codes, and added support for basic HTML.
      This resolves [issue #559](https://bitbucket.org/okapiframework/okapi/issues/559).

* PdfFilter

    * New PDF filter. Extraction only.

* YAML Filter

    * Fix [Issue #555](https://bitbucket.org/okapiframework/okapi/issues/555): YAML filter skips single quote at beginning of string
    * Fix [Issue ##556](https://bitbucket.org/okapiframework/okapi/issues/556): YAML filter should escape sequences before
      passing to subfilters.

## Applications

* CheckMate

    * Now sessions with very large configurations can be saved. This fix
      will produces session files (`.qcs`) that are not backward
      compatible with previous version of CheckMate if the size of the
      configuration is larger than about 21K. The fix produces backward
      compatible files under that size. This resolves [issue #548](https://bitbucket.org/okapiframework/okapi/issues/548).

* Rainbow

    * Fixed [issue #559](https://bitbucket.org/okapiframework/okapi/issues/559): Batch configurations will now map `.mqxliff`
      files to the XLIFF Filter by default.
    * Changed the default encodings of the UI to UTF-8 when running on a
      Macintosh or on Unix/Linux.

## Connectors

* Microsoft Translation Hub

    * The connector will now correctly handle batch queries when
      provided with content in excess of 10 segments or 10,000 characters.
      Callers should no longer need to do additional batching of their own.
    * The `query(String)` method will now send content for
      translation as a raw string, without doing additional tag
      processing. This brings the implementation in line with the javadoc,
      and is intended to be used in cases when a caller wishes to send raw
      HTML to Translation Hub. The `query(TextFragment)` method
      will continue to replace inline codes with dummy HTML tags before
      calling the API. A new `batchQueryText(List<String>)`
      method has been added to perform batch queries of text without
      additional processing, to complement the existing `batchQuery(List<TextFragment>)`
      method.
    * Improve error handling, including handling of invalid segments,
      latency spikes, and the `X-MS-Trans-Info` diagnostic header.
    * Content using the `zh-hk` locale will now be sent as
      Traditional Chinese rather than Simplified Chinese.
    * Ensured the language codes starting with `sr-Cyrl`
      (Serbian in Cyrillic) are mapped properly to the Microsoft internal
      code.
    * Ensured the language codes `in` are mapped to `id`
      as expected by Microsoft internal code.

* OpenTran

    * Removed connector for the (defunct) OpenTran service.

## General

    * Add ability to add ICU4J segmentation rules via SRX configuration
      option "`icu4jBreakRules`". Add `okapi_default_icu4j.srx`
      file with enhanced rules.
    * Now using the Okapi XLIFF 2 library version 1.1.4.

# Changes from M13 to M14

## Applications

* Rainbow

    * Fixed input root setting when running from the command-line.
    * Fixed missing command-line IDs for Translation Kit creation and
      post-processing pre-defined pipelines.

* Tikal

    * Added the `-bpt` option in the Leverage Files from
      Moses command.
    * Added the `-to` option in the Leverage Files from
      Moses command.
    * Made the key parameter of the MyMemory connector optional (kept it
      for backward compatibility)
    * Updated bash script (tikal.sh) to allow more than 9 arguments.
    * Added the `-od` option in the Extract Files and Merge
      Files commands.
    * Added the `-sd` option in the Merge Files commands.

* CheckMate

    * Added support for TXML files.

## Filters Plugin for OmegaT

* Added support for TXML files.
* Updated filter names to indicate they are from the Okapi plugin
  (as we may have duplicated filters with the default OmegaT filters)

## Steps

* Id-Based Copy Step

    * Added the options to mark the leveraged text units as
      non-translatable.
    * Added the option to mark the leveraged translation as
      approved.

* Rainbow Translation Kit Creation Step

    * Added the support for Versified Text + RTF packages.
    * The XINI package output now support pre-translated items.
    * Changed the default output directory from `${rootDir}`
      to `${inputRootDir}`.
    * Continued to improved the experimental XLIFF 2.0 package.
    * Updated OmegaT package so the `<dictionnary_dir>`
      element required for OmegaT 2.5 is generated.
    * Fixed handling of input file without filter for XLIFF+RTF and
      Versified+RTF outputs.
    * Fixed the problem with the destination folder when merging
      several manifests one after the other.

* XML Validation Step

    * Fixed case of relative path for included DTDs.

* Translation Comparison Step

    * Added the option to use generic representation for inline
      codes.
    * Improved the error handling.

* Segmentation Step

    * Added suport for `${inpuRootDir}`.

* Leveraging Step

    * Added suport for `${inpuRootDir}` for the TMX output.

* Microsoft Batch Translation Step

    * Added suport for `${inpuRootDir}` for the TMX output.

* Moses Leveraging Step

    * Added the option to not use `<g>` notation
      in new `<alt-trans>` elements in output.

## Filters

* Added the TXML Filter

    * It allows you to process Wordfast pro
      native translation files. This filter is early BETA for now.

* HTML Filter

    * Fixed un-escaping for '&amp;' issue with non-extractable
      attribute values.
    * Added the `escapeCharacters` option to allow
      output using character entity references.

* Versified Text Filter

    * Added support for bilingual variant of the format.

* XLIFF Filter

    * Added the option to not use `<g>` notation
      in new `<alt-trans>` elements in output.
    * Modified the generation of group IDs to take into account the
      ones existing in the input file. This fixes [issue #185](https://bitbucket.org/okapiframework/okapi/issues/185).
    * Added support for cases where a `<seg-source>`
      element has no `<mrk>` element: it's treated
      as a single-segment entry.

* RTF Filter

    * Updated filter to strip out null characters present in some
      varities of RTF files.

* TMX Filter

    * Fixed the problem of some attribute values not being escaped
      properly in some cases.

* XML Filter

    * Added the option `extractIfOnlyCodes` to allow
      entries with only codes or whitespace but no text to not be
      extracted. For backward compatibility this option is set to
      true.

* IDML Filter

    * Added support for stories nested inside other stories.
    * Fixed issue with empty `<Content>` element, and tested
      with more complex CS5 files.

* RainbowTKit Filter

    * Improve error handling in Rainbow translation kit
      post-processing.

* Table Filter

    * Add support for adding qualifiers when needed for the CSV
      output. This fixes [issue #187](https://bitbucket.org/okapiframework/okapi/issues/187).

* TTX Filter

    * Added the option of including or not unsegmented text and
      added the pre-defined configuration `preSegmented`
    * Changed the parsing so the IDs are unique within each text
      unit rather than reset for each segment

* Properties Filter

    * Added support for sub-filters.
    * Added the pre-defined configuration `okf_properties-html-subfilter`

## Connectors

* MyMemory TM Connector

    * Changed the code to use the REST interface rather than the
      SOAP interface.

* Microsoft Translator Connector

    * Changed the `queryList()` method to `batchQuery()` so it can be
      used through the `IQuery` interface.
    * The original score is now set in score, while the
      re-calculated score is the combined score.

* Google MT v2 Connector

    * Implemented the new version (v2) of the **paid service**
      of Google Translate. See http://code.google.com/apis/language/translate/v2/getting_started.html
      for more details on how to obtain an API Key to use this
      connector. The connector implements both `query()` and
      `batchQuery()`.

## Libraries

* Continued to improve the XLIFF 2.0 experimental library.
* Updated `IQuery` to include `batchQuery()` and `batchLeverage()`.
* Updated `QueryResult` to include the new `combinedScore` and `quality`
  fields.
* Changed various classes to use the combined score rather than the
  score for their filtering.
* Made various fixes and improvements to the core libraries.


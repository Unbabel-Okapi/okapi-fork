# Changes from M3 to M4

## Filters

* XLIFF Filter

    * Added property for target-language and option to add
      it. Changed some of the language selection behaviors and set
      fall-back to ID option to false.

* OpenXML Filter

    * Fixed several bugs (MS Office 2007 documents)

* Added the JSON Filter

    * to support for example AJAX or Palm WebOS applications.

* Added the PHP Content Filter

    * to support PHP include files.

* Others

    * Added default DITA configuration to the XML Filter.
    * Fixed several issues with the TS, Table, TMX, and XLIFF filters.
    * Added `whiteSpaces` ITS extension support in the XML
      Filter.

## Library, Translation resources

* All the TM and MT connectors have been moved to the package `net.sf.okapi.connectors`.
* Modified the OpenTran connector to use the REST interface instead
  of RCP.
* Added the connector to the MyMemory server (http://mymemory.translated.net)
* Improved Google MT connector.
* Improved GlobalSight TM connector for inline codes, and adjusted
  it for GS version 7.1.6.
* Added Pensieve TM engine and its connector.
* Added the connector for the open-source Apertium MT system web
  service (http://wiki.apertium.org/wiki/Main_Page)
* Changed language identification from `String` to `LocaleId` objects
  across the whole framework.

## Steps and Rainbow utilities

* Added the SimpleTM2TMX Step
* Added Import and Export utilities for SimpleTM files.
* Continued improving the Tokenization and WordCount steps.
* Implement an option to select the XSLT processor to use with the "XSL
  Transformation" utility.
* Updated the "Translation Package Creation" utility to
  select from several resources for the pre-translation options, and
  to allow specifying threshold instead "exact match only".
* Updated the "Text Rewriting" utility to select from
  several resources for the translation options.
* Added the FormatConversion Step
* Improved inline compatibility in projects generated for OmegaT.

## Applications

* Tikal

    * Added support for accessing the MyMemory repository (`-mm` option)
    * Corrected display of extended characters on the console for some
      languages/platforms.
    * Added threshold and max-hits options for TM query command (`-opt` option)
    * Added a command to create PO files from any input (`-2po` command).
    * Added a command to create TMX files from any input (`-2tmx` command).
    * Added a command to create Table files from any input (`-2tbl` command).
    * Added capability to query a Pensieve TM (`-pen` option).
    * Added support for accessing GlobalSight TM servers (`-gs` option).
    * Added support for accessing Apertium MT servers (`-apertium` option).
    * Added segmentation and leveraging options for the extraction command.
    * Added a commands to import any file into a Pensieve TM (`-imp` command).
    * Added a command to export a Pensieve TM to a TMX file (`-exp` command).


# Changes from M12 to M13

## Applications

* Rainbow

    * Added the user preference "Use the last session's locales and
      encodings as defaults".

* CheckMate

    * Fixed the null pointer issue when target pattern different from
      the source match were not found.
    * Added check of the opening/closing sequence for inline codes. This
      check is done only if no other error is found in the segment.
    * Added the option "Try to guess opening/closing types for
      placeholder codes" for the codes.

* Tikal

    * Added the `-a` command to add translations to a resource.
    * Fixed usage screen for minor details.

* Longhorn

    * Longhorn is now part of the regular releases. Longhorn is a server
      application that allows you to execute batch configurations remotely
      on any set of input files. Batch configurations which include
      pre-defined pipelines and filter configurations, can be exported
      from Rainbow. For more information see http://www.opentag.com/okapi/wiki/index.php?title=Longhorn

## Steps

* Rainbow Translation Kit Creation Step

    * Changed the XLIFF and XLIFF + RTF output behavior so single
      and double quotes are not escaped in element content.
    * Changed the XLIFF + RTF output to use short notation for empty
      inline codes. \
      <u>**IMPORTANT**</u>: This updated notation may have impact
      on TM matches using the previous notation. Some TM tools will
      have coded the two parts `<x id="1">` and `</x>`
      as two separate inline codes (because they are styled as two
      spans of tw4winInternal) and will see the new notation `<x id="1"/>`
      as a single code. For Trados-2007 TM
      for example you may want to update your TM by replacing the two
      adjacent codes with a single one. Export the TM to TMX 1.4b,
      replace `<bpt\s.*?>&lt;x id=&quot;(\d\d?)&quot;&gt;</bpt><ept\s.*?>&lt;/x&gt;</ept>`
      by `<ph>&lt;x id=&quot;\1&quot;/></ph>` and create a
      new TM based on the modified notation. That new TM should yield
      segments that will match with the new notation.
    * Added the option "Create a ZIP file for the package"
    * Continued experimental implementation of XLIFF 2.0.

* Rainbow Translation Kit Merging Step

    * Added the option to generate raw document events or filter
      events.
    * Added support for post-processing directly `.rkp` zipped files.

* Extraction Verification Step

    * Improved support for bilingual file formats.

* Search and Replace Step

    * Fixed UI so the list of expression is not too high and drive
      button out of the screen.
    * Added the Microsoft Batch Translation Step: it allows to batch
      translate resources using the Microsoft Translator TM/MT engine
      (the Collaborative Translations Framework)
    * Added the Microsoft Batch Submission Step: it allows to submit
      human or post-edited translations to the Microsoft Collaborative
      Translations Framework. Those translations can be retrieved later.

## Filters

* XLIFF Filter

    * Added internal flag to output un-escaped single and double
      quotes.
    * Fixed filter-writer to take into account un-segmented entries
      with existing translation in the layered output (e.g. RTF).
    * Fixed bug of un-balanced opening and closing codes segmented
      entry when codes are nested over parts.
    * Updated how outer data of empty inline codes are stored (e.g.
      now uses `<x id='1'/>` instead of `<x id='1'></x>`)

* PHP Content Filter

    * Fixed the case of multiple placeholder codes with the same ID.
      This fixes [issue #179](https://bitbucket.org/okapiframework/okapi/issues/179).

* IDML Filter

    * Implemented extraction of master spreads (as an option set by
      default).
    * Fixed bug of in the simplification of trailing inline codes
      after a single character.

## Connectors

* Microsoft Translator Connector

    * Changed the name from Microsoft MT Connector (since it handles
      more than MT matches)
    * Implemented `ITMQuery` and multiple results.

## Libraries

* Added support for quote mode in `XMLEncoder`.
* Upgraded ICU4J library from 4.6 to 4.8.
* Added `PIPELINE_PARAMETERS` event.
* Upgraded the SWT library from 3.6.1 to 3.7.


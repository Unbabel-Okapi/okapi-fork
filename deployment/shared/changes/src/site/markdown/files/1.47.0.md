# Changes from 1.46.0 to 1.47.0

<!-- MACRO{toc} -->

## Core


## Connectors

## Filters

* OpenXML Filter

    * Fixed: responsive table dialog capabilities clarified:
    [issue#1337](https://bitbucket.org/okapiframework/okapi/issues/1337).
    * Fixed: nested content with complex fields handling clarified:
    [issue#1341](https://bitbucket.org/okapiframework/okapi/issues/1341).

* XLIF 2.0 Filter
    * Added an option to allow the filter to continue processing
      files that contain invalid target data, instead clearing
      the target and resetting its state to `initial`.

## Libraries

## Steps

## TMs

## Applications

## OSes

## Installation

## General

## Plugins


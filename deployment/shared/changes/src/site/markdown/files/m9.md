# Changes from M8 to M9

## Applications

* Rainbow

    * Translation package Post-Processing utility:
        * Fixed the bug where pre-translated XLIFF entries with
          `translate='no'` could not be merged back properly, for example
          for PO files.
    * Added the user option "Always show the Log when starting a
      process".

* Tikal

    * Fixed the bug in the Merge command where pre-translated XLIFF
      entries with translate='no' could not be merged back properly, for
      example for PO files.
    * Switched help to use the wiki.

* Ratel

    * Windows position and size are now saved for the next session.

* CheckMate

    * Added capability to save and load configurations outside the
      session.
    * Improved pattern checks defaults and processing.
    * Added support for short vs. long text in text length verification
      (new Length tab)
    * Added experimental support for terminology verification.
    * Added support for exceptions in verification of double-words.
    * Added some limited support for string-based term verification.

## Connectors

* Added `batchQuery` method to the `IQuery` interface.
* Added `leverage` method to the `IQuery` interface.

* Open-Tran Connector

    * Changed implementation to use the REST API instead of the
      XML-RPC.
    * Improved support for queries with inline codes.

* SimpleTM Connector

    * <u>**IMPORTANT**</u>: Changed the H2 database dependency
      from version 1.1.103 (`.data.db` files) to 1.2.135 (`.h2.db` files),
      this breaks backward compatibility: the new SimpleTM connector
      cannot open the old `.data.db` files. To convert an older TM: Use
      a M8 or prior version of Rainbow to run the **SimpleTM to TMX**
      step to export your database to TMX. Then, Use this version of
      Rainbow to run the **Generate SimpleTM** step to convert
      your TMX document into a new `.h2.data` file.

## Steps

* Added the Resource Simplifier Step

    * It modifies normal reources of
     filter events into simpler resources for some third-party tools.

* Added the XLIFF Spitter Step

    * It splits several `<file>` inside an XLIFF documents into separate
      documents.

* Added the Id-Based Aligner Step

    * It aligns text units from two input files, based on their unique IDs (resname).

* Added the XML Validation Step

    * It performs well-formness XML
      verification and optionally, DTD or schema validation.

* Sentence Aligner Step

    * Updated so entries with empty text are skipped and don't cause
      an error.

* Diff Leverage Step

    * Added support for 3 input files: new source, old source, old
      translation. The second and third files must have the same text
      units (same number and same order).

## Filters

* Modified several filters to generate unique extraction ids in
  non-text-unit events.

* Vignette Filter

    * Added support for monolingual documents.

* XML Filter

    * Fixed the bug where text extracted from attribute values was
      not processed for the codeFinder option.

## Libraries

* Implemented the Appendable and CharSequence interfaces for
  TextFragment.
* <u>**IMPORTANT:**</u> Changed `TextFragment.toString()`
  to return the coded text instead of the original content of the
  fragment. The previous behavior of `toString()` is now
  accessible using ` text()`.
* The `net.sf.okapi.lib.extra.pipelinebuilder` package
  has been added. It allows you to easily script run pipelines, for
  example using Jython.

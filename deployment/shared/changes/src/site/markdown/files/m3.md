# Changes from M2 to M3

* The build system has been completely redone and now uses Maven as its
        main builder. This has resulted in several changes in the structure of
        the Okapi classes, and in the way the files are distributed.

## Filters

* Added the TS Filter (beta) for Qt translation files.

    * Fixed handling of fuzzy flag for plural entries in the PO filter.
    * Fixed handling of `approved`, `state` and
      `coord` properties in the XLIFF Filter.

* Improved XML Filter

    * Improved rewriting of document type subset declaration.
    * Added support for protecting custom entity references.
    * Added support for ID defined using `xml:id` or the
      `idPointer` ITS extension feature.

* Properties Filter

    * Change the default configuration to always escape output.
    * Added pre-defined configuration for non-escaped output.

* Fixed various issues in the OpenXML Filter (docx, pptx, etc.), and
      PO Filter.

## Libraries

* The Google MT connector has been enhanced to have the inline codes
  taken into account, not simply pushed to the end of the text.
* Fixed one error in default segmentation rules.
* Added a connector component for the Translate Toolkit TM server.
* Added steps such as Word-count and Tokenizer.

## Applications

* The command-line tool Tikal has been added.

* Rainbow *(okapi-apps distribution only)*

    * Improved handling of un-approved translations in TMX generated
      during a translation package creation.
    * Added option to choose to merge only approved translations in
      translation package post-processing.


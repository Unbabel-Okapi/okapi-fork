package net.sf.okapi.common.filters;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.skeleton.ISkeletonWriter;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Christian Hargraves
 * Date: Jun 26, 2009
 * Time: 12:41:47 PM
 */
public class StubFilter extends AbstractFilter {

	private static final String MIMETYPE = "text/foo";

	private EncoderManager encoderManager;

    public StubFilter() {
        super();
    }
	
    public String getName() {
        return "foobar";
    }
	
	@Override
    public String getDisplayName () {
		return "Stub Filter";
	}

    @Override
    public void open(RawDocument input) {
    }

    @Override
    public void open(RawDocument input, boolean generateSkeleton) {
    }

    public void close() {
    }

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public Event next() {
        return null;
    }
    @Override
    public void cancel() {
    }
    public ParametersStub getParameters() {
        return new ParametersStub();
    }

    @Override
    public void setParameters(IParameters params) {
    }

	@Override
    public void setFilterConfigurationMapper (IFilterConfigurationMapper fcMapper) {
	}

    @Override
    public ISkeletonWriter createSkeletonWriter() {
        return null;
    }

    @Override
    public IFilterWriter createFilterWriter() {
        return null;
    }

    @Override
    public String getMimeType() {
        return MIMETYPE;
    }

    @Override
    public List<FilterConfiguration> getConfigurations() {
 		List<FilterConfiguration> list = new ArrayList<>();
		list.add(new FilterConfiguration(getName(),
			MIMETYPE,
			getClass().getName(),
			"Regex Default",
			"Default foo configuration."));
		list.add(new FilterConfiguration(getName()+"-srt",
			MIMETYPE,
			getClass().getName(),
			"STR Sub-Titles",
			"Configuration for SRT (Sub-Rip Text) sub-titles files.",
                "srt.fprm"));
		return list;
    }

	@Override
    public EncoderManager getEncoderManager () {
		if ( encoderManager == null ) {
			encoderManager = new EncoderManager();
			encoderManager.setAllKnownMappings();
		}
		return encoderManager;
	}

	static class ParametersStub implements IParameters{

        public ParametersStub() { reset(); }

        public void reset() {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        public void fromString(String data) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public void load(URL inputURL, boolean ignoreErrors) {
            //To change body of implemented methods use File | Settings | File Templates.
        }
        
        @Override
		public void load(InputStream inStream, boolean ignoreErrors) {
			//To change body of implemented methods use File | Settings | File Templates.
		}

        public void save(String filePath) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public String getPath() {
            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public void setPath(String filePath) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public boolean getBoolean(String name) {
            return false;  //To change body of implemented methods use File | Settings | File Templates.
        }

        public String getString(String name) {
            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public int getInteger(String name) {
            return 0;  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public ParametersDescription getParametersDescription() {
            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }

		@Override
		public void setBoolean (String name, boolean value) {
			// TODO Auto-generated method stub
		}

		@Override
		public void setInteger (String name, int value) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setString (String name, String value) {
			// TODO Auto-generated method stub
		}
		
		@Override
		public String toString () {
			return "Dummy filter custom filter settings file";
		}
    }

}

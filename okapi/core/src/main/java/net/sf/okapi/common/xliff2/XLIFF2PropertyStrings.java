/*===========================================================================
  Copyright (C) 2019 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================*/

package net.sf.okapi.common.xliff2;

public class XLIFF2PropertyStrings {

    public static final String CAN_COPY = "canCopy";
    public static final String CAN_DELETE = "canDelete";
    public static final String CAN_OVERLAP = "canOverlap";
    public static final String CAN_REORDER = "canReorder";
    public static final String CAN_RESEGMENT = "canResegment";
    public static final String CATEGORY = "category";
    public static final String COPY_OF = "copyOf";
    public static final String DATA = "data";
    public static final String DATA_REF = "dataRef";
    public static final String DIR = "dir";
    public static final String DISP = "disp";
    public static final String EQUIV = "equiv";
    public static final String HREF = "href";
    public static final String NAME = "name";
    public static final String ORDER = "order";
    public static final String REF = "ref";
    public static final String SRC_DIR = "srcDir";
    public static final String SRC_LANG = "srcLang";
    public static final String STATE = "state";
    public static final String SUB_FLOWS = "subFlows";
    public static final String SUB_STATE = "subState";
    public static final String SUB_TYPE = "subType";
    public static final String TRG_LANG = "trgLang";
    public static final String TRG_DIR = "trgDir";
    public static final String TYPE = "type";
    public static final String VALUE = "value";
    public static final String VERSION = "version";

    // Custom parameters
    public static final String EXTENDED_ATTRIBUTE_PREFIX = "extendedAttribute.";
    public static final String EXTENDED_NAMESPACE_PREFIX = "extendedNamespace.";
    public static final String EXTENDED_ATTRIBUTE_DELIMITER = "==";

    public static final String DATA_DIR = "dataDir";
    public static final String TRANSLATE = "translate";
    public static final String MTAG = "mtag";
    public static final String TERM = "term";
    public static final String COMMENT = "comment";
}

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.xmlunit.diff.DifferenceEvaluators;
import org.xmlunit.matchers.CompareMatcher;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(JUnit4.class)
public class TestParagraphSimplifier {
	private final FileLocation root;
	private final ConditionalParameters defaultParameters;
	private final XMLFactories xmlfactories;
	private final PresetColorValues presetColorValues;
	private final PresetColorValues highlightColorValues;
	private final StyleDefinitions styleDefinitions;
	private final StyleOptimisation.Bypass bypassStyleOptimisation;
	private final StyleOptimisation powerpointStyleOptimisation;
	private final ContentCategoriesDetection nonApplicableContentCategoriesDetection;
	private final ContentCategoriesDetection defaultContentCategoriesDetection ;


	public TestParagraphSimplifier() {
		this.defaultParameters = new ConditionalParametersBuilder()
			.cleanupAggressively(false)
			.addTabAsCharacter(false)
			.lineSeparatorAsChar(false)
			.build();
		this.xmlfactories = new XMLFactoriesForTest();
		this.root = FileLocation.fromClass(getClass());
		this.presetColorValues = new PresetColorValues.Default();
		this.highlightColorValues = new PresetColorValues.Default(Collections.emptyList());
		this.styleDefinitions = new StyleDefinitions.Empty();
		this.bypassStyleOptimisation = new StyleOptimisation.Bypass();
		this.powerpointStyleOptimisation = new StyleOptimisation.Default(
			new StyleOptimisation.Bypass(),
			this.defaultParameters,
			this.xmlfactories.getEventFactory(),
			presetColorValues, highlightColorValues, Namespaces.DrawingML.getQName("pPr", Namespace.PREFIX_A),
			Namespaces.DrawingML.getQName("defRPr", Namespace.PREFIX_A),
			Collections.emptyList(),
			styleDefinitions
		);
		this.nonApplicableContentCategoriesDetection = new ContentCategoriesDetection.NonApplicable();
		this.defaultContentCategoriesDetection = new ContentCategoriesDetection.Default(LocaleId.ENGLISH);
	}

	@Test
	public void testSimplifier() throws Exception {
		simplifyAndCheckFile("document-simple.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
		simplifyAndCheckFileAggressive("document-simple.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
	}

	@Test
	public void testDontMergeWhenPropertiesDontMatch() throws Exception {
		simplifyAndCheckFile("document-prop_mismatch.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
	}

	@Test
	public void testWithTabs() throws Exception {
		simplifyAndCheckFile("document-multiple_tabs.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
	}

	@Test
	public void testHeaderWithConsecutiveTabs() throws Exception {
		simplifyAndCheckFile("header-tabs.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
	}

	@Test
	public void testTextBoxes() throws Exception {
		simplifyAndCheckFile("document-textboxes.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
	}

	@Test
	public void testRuby() throws Exception {
		simplifyAndCheckFile("document-ruby.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
	}

	@Test
	public void testSlide() throws Exception {
		simplifyAndCheckFile("slide-sample.xml", powerpointStyleOptimisation, nonApplicableContentCategoriesDetection);
	}

	@Test
	public void testInstrText() throws Exception {
		simplifyAndCheckFile("document-instrText.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
	}

	@Test
	public void testAltContent() throws Exception {
		simplifyAndCheckFile("document-altcontent.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
	}

	@Test
	public void testPreserveSpaceReset() throws Exception {
		simplifyAndCheckFile("document-preserve.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
	}

	@Test
	public void testStripLastRenderedPagebreak() throws Exception {
		simplifyAndCheckFile("document-pagebreak.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
	}

	@Test
	public void testStripSpellingGrammarError() throws Exception {
		simplifyAndCheckFile("document-spelling.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
	}

	@Test
	public void testLangAttributeAndEmptyRunPropertyMerging() throws Exception {
		simplifyAndCheckFile("document-lang.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
	}

	@Test
	public void testDontConsolidateMathRuns() throws Exception {
		simplifyAndCheckFile("slide-formulas.xml", powerpointStyleOptimisation, nonApplicableContentCategoriesDetection);
	}

	@Test
	public void testAggressiveSpacingTrimming() throws Exception {
		simplifyAndCheckFile("document-spacing.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
		simplifyAndCheckFileAggressive("document-spacing.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
	}

	@Test
	public void testAggressiveVertAlignTrimming() throws Exception {
		simplifyAndCheckFileAggressive("document-vertAlign.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
	}

	@Test
	public void testGoBackBookmark() throws Exception {
		simplifyAndCheckFile("document-goback.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
	}

	@Test
	public void testTab() throws Exception {
		simplifyAndCheckFileTabAsChar("document-tab.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
	}

	@Test
	public void testFonts() throws Exception {
		simplifyAndCheckFile("document-fonts.xml", bypassStyleOptimisation, defaultContentCategoriesDetection);
	}

	@Test
	public void testLineSeparatorSlide() throws Exception {
		final ConditionalParameters conditionalParameters = new ConditionalParametersBuilder()
			.cleanupAggressively(true)
			.addTabAsCharacter(false)
			.lineSeparatorAsChar(true)
			.lineSeparatorReplacement('\n')
			.build();
		simplifyAndCheckFileLineSeparatorAsChar(
			"slide-linebreak.xml",
			conditionalParameters,
			new StyleOptimisation.Default(
				this.bypassStyleOptimisation,
				conditionalParameters,
				this.xmlfactories.getEventFactory(),
                this.presetColorValues,
				this.highlightColorValues,
				Namespaces.DrawingML.getQName("pPr"),
				Namespaces.DrawingML.getQName("defRPr"),
				Collections.emptyList(),
				styleDefinitions
			),
			this.nonApplicableContentCategoriesDetection
		);
	}

	@Test
	public void testLineSeparatorSlide2028() throws Exception {
		final ConditionalParameters conditionalParameters = new ConditionalParametersBuilder()
			.cleanupAggressively(true)
			.addTabAsCharacter(false)
			.lineSeparatorAsChar(true)
			.lineSeparatorReplacement('\u2028')
			.build();
		simplifyAndCheckFileLineSeparatorAsChar(
			"slide-linebreak-2028.xml",
			conditionalParameters,
			new StyleOptimisation.Default(
				new StyleOptimisation.Bypass(),
				conditionalParameters,
				this.xmlfactories.getEventFactory(),
                this.presetColorValues,
				this.highlightColorValues,
				Namespaces.DrawingML.getQName("pPr"),
				Namespaces.DrawingML.getQName("defRPr"),
				Collections.emptyList(),
				styleDefinitions
			),
			this.nonApplicableContentCategoriesDetection
		);
	}

	// Simplify
	//   src/test/resources/parts/simplifier/[name]
	// And compare to
	//   src/test/resources/gold/parts/simplifier/[name]
	private void simplifyAndCheckFile(String name, StyleOptimisation styleOptimisation, ContentCategoriesDetection contentCategoriesDetection) throws Exception {
		simplifyAndCheckFile(
			name,
			"/gold/parts/simplifier/",
			this.defaultParameters,
			styleOptimisation,
			contentCategoriesDetection
		);
	}
	private void simplifyAndCheckFileAggressive(String name, StyleOptimisation styleOptimisation, ContentCategoriesDetection contentCategoriesDetection) throws Exception {
		simplifyAndCheckFile(
			name,
			"/gold/parts/simplifier/aggressive/",
			new ConditionalParametersBuilder()
				.cleanupAggressively(true)
				.addTabAsCharacter(false)
				.lineSeparatorAsChar(false)
				.build(),
				styleOptimisation,
			contentCategoriesDetection
		);
	}
	private void simplifyAndCheckFileTabAsChar(String name, StyleOptimisation styleOptimisation, ContentCategoriesDetection contentCategoriesDetection) throws Exception {
		simplifyAndCheckFile(
			name,
			"/gold/parts/simplifier/tabAsChar/",
			new ConditionalParametersBuilder()
				.cleanupAggressively(false)
				.addTabAsCharacter(true)
				.lineSeparatorAsChar(false)
				.build(),
			styleOptimisation,
			contentCategoriesDetection
		);
	}

	private void simplifyAndCheckFileLineSeparatorAsChar(
		String name,
	 	ConditionalParameters conditionalParameters,
		StyleOptimisation styleOptimisation,
		ContentCategoriesDetection contentCategoriesDetection
	) throws Exception {
		simplifyAndCheckFile(
			name,
			"/gold/parts/simplifier/lbAsChar/",
			conditionalParameters,
			styleOptimisation,
			contentCategoriesDetection
		);
	}

	private void simplifyAndCheckFile(
		String name,
		String goldDir,
		ConditionalParameters params,
		StyleOptimisation styleOptimisation,
		ContentCategoriesDetection contentCategoriesDetection
	) throws Exception {
		final Path temp = simplifyFile(name, params, styleOptimisation, contentCategoriesDetection);

		final Path goldFile = FileLocation.fromClass(getClass()).in(goldDir + name).asPath();
		final String goldContent = new String(Files.readAllBytes(goldFile), StandardCharsets.UTF_8);
		final String tempContent = new String(Files.readAllBytes(temp), StandardCharsets.UTF_8);

		assertThat(
			tempContent,
			CompareMatcher.isSimilarTo(goldContent).withDifferenceEvaluator(DifferenceEvaluators.ignorePrologDifferences()))
		;

		Files.delete(temp);
	}

	private Path simplifyFile(
		String name,
		ConditionalParameters params,
	  	StyleOptimisation styleOptimisation,
		ContentCategoriesDetection contentCategoriesDetection
	) throws Exception {
		XMLEventReader xmlReader = this.xmlfactories.getInputFactory().createXMLEventReader(
				root.in("/parts/simplifier/" + name).asInputStream(), "UTF-8");
		Path temp = Files.createTempFile("simplify", ".xml");
		//System.out.println("Writing simplified " + name + " (aggressive=" + aggressiveTrimming + ") to " + temp);
		XMLEventWriter xmlWriter = this.xmlfactories.getOutputFactory().createXMLEventWriter(
				Files.newBufferedWriter(temp, StandardCharsets.UTF_8));

		ParagraphSimplifier simplifier = new ParagraphSimplifier(
			xmlReader,
			xmlWriter,
			presetColorValues,
			this.highlightColorValues,
			this.xmlfactories.getEventFactory(),
			params,
			this.styleDefinitions,
			styleOptimisation,
			contentCategoriesDetection
		);

		simplifier.process();
		xmlReader.close();
		xmlWriter.close();
		return temp;
	}
}
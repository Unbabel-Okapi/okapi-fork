/*
 * =============================================================================
 * Copyright (C) 2010-2023 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.encoder.EncoderContext;
import net.sf.okapi.common.encoder.QuoteMode;
import net.sf.okapi.common.encoder.XMLEncoder;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.common.skeleton.GenericSkeleton;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;

final class NumberingPart extends TranslatablePart {
    private static final Pattern levelNumberPattern = Pattern.compile("%\\d");
    private final NumberingDefinitions numberingDefinitions;
    private final IdGenerator textUnitIds;
    private final XMLEncoder attributeValueEncoder;
    private String subDocumentId;
    private Iterator<Event> filterEventsIterator;

    NumberingPart(
        final Document.General generalDocument,
        final ZipEntry entry,
        final NumberingDefinitions numberingDefinitions
    ) {
        super(generalDocument, entry);
        this.numberingDefinitions = numberingDefinitions;
        this.textUnitIds = new IdGenerator(entry.getName(), IdGenerator.TEXT_UNIT);
        this.attributeValueEncoder = new XMLEncoder(OpenXMLFilter.ENCODING.name(), "\n", true, true, false, QuoteMode.ALL);
    }

    @Override
    public Event open() throws IOException, XMLStreamException {
        this.subDocumentId = this.generalDocument.nextSubDocumentId();
        formFilterEvents();
        this.filterEventsIterator = this.filterEvents.iterator();
        return createStartSubDocumentEvent(this.generalDocument.documentId(), this.subDocumentId);
    }

    private void formFilterEvents() {
        addToDocumentPartAndFlush(this.numberingDefinitions.startEvents());
        formFilterEventsFrom(this.numberingDefinitions.abstractNumberingDefinitionsIterator());
        formFilterEventsFrom(this.numberingDefinitions.numberingDefinitionsIterator());
        addToDocumentPartAndFlush(this.numberingDefinitions.endEvents());
        this.filterEvents.add(new Event(EventType.END_SUBDOCUMENT, new Ending(this.subDocumentId)));
    }

    private void formFilterEventsFrom(final Iterator<NumberingDefinition> iterator) {
        while (iterator.hasNext()) {
            final NumberingDefinition nd = iterator.next();
            if (nd.referenced()) {
                addToDocumentPartAndFlush(nd.eventsBeforeNumberingLevels());
                final Iterator<NumberingLevel> nli = nd.numberingLevelsIterator();
                while (nli.hasNext()) {
                    final NumberingLevel nl = nli.next();
                    if (nl.numberingTextPresent() && nl.numberingText().referenced()) {
                        addToDocumentPartAndFlush(nl.eventsBeforeText());
                        if (nl.numberingText().value().isEmpty()) {
                            addToDocumentPartAndFlush(nl.numberingText().asEvents());
                        } else {
                            final ITextUnit textUnit = textUnitFor(nl.numberingText());
                            this.filterEvents.add(new Event(EventType.TEXT_UNIT, textUnit));
                        }
                        addToDocumentPartAndFlush(nl.eventsAfterText());
                    } else {
                        addToDocumentPartAndFlush(nl.asEvents());
                    }
                }
                addToDocumentPartAndFlush(nd.eventsAfterNumberingLevels());
            } else {
                addToDocumentPartAndFlush(nd.asEvents());
            }
        }
    }

    private void addToDocumentPartAndFlush(final List<XMLEvent> events) {
        events.forEach(e -> addEventToDocumentPart(e));
        flushDocumentPart();
    }

    private ITextUnit textUnitFor(final NumberingText numberingText) {
        final ITextUnit textUnit = new TextUnit(this.textUnitIds.createId());
        textUnit.setSource(new TextContainer(textFragmentFor(numberingText)));
        textUnit.setMimeType(OpenXMLFilter.MIME_TYPE);
        final GenericSkeleton skel = new GenericSkeleton();
        skel.append("<");
        skel.append(stringFor(numberingText.startElement().getName()));
        final Iterator<?> iterator = numberingText.startElement().getAttributes();
        while (iterator.hasNext()) {
            final Attribute attribute = (Attribute) iterator.next();
            skel.append(" ");
            skel.append(stringFor(attribute.getName()));
            skel.append("=\"");
            if (attribute.getValue().equals(numberingText.value())) {
                skel.addContentPlaceholder(textUnit);
            } else {
                skel.append(stringFor(attribute.getValue()));
            }
            skel.append("\"");
        }
        skel.append("/>");
        textUnit.setSkeleton(skel);
        return textUnit;
    }

    private TextFragment textFragmentFor(final NumberingText numberingText) {
        final TextFragment tf = new TextFragment();
        final Matcher m = levelNumberPattern.matcher(numberingText.value());
        int textStart = 0;
        while (m.find()) {
            tf.append(numberingText.value().substring(textStart, m.start()));
            tf.append(
                TextFragment.TagType.PLACEHOLDER,
                "x-level-number;",
                numberingText.value().substring(m.start(), m.end())
            );
            textStart = m.end();
        }
        if (textStart < numberingText.value().length()) {
            tf.append(numberingText.value().substring(textStart, numberingText.value().length()));
        }
        return tf;
    }

    private static String stringFor(final QName name) {
        final StringBuilder sb = new StringBuilder();
        if (null != name.getPrefix() && !"".equals(name.getPrefix())) {
            sb.append(name.getPrefix()).append(":");
        }
        sb.append(name.getLocalPart());
        return sb.toString();
    }

    private String stringFor(final String attributeValue) {
        return this.attributeValueEncoder.encode(attributeValue, EncoderContext.INLINE);
    }

    @Override
    public boolean hasNextEvent() {
        return this.filterEventsIterator.hasNext();
    }

    @Override
    public Event nextEvent() {
        return this.filterEventsIterator.next();
    }

    @Override
    public void close() {
    }

    @Override
    public void logEvent(final Event e) {
    }
}


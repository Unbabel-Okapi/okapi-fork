/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

interface WorksheetFragments {
    List<XMLEvent> events();
    Set<Integer> hiddenRows();
    Set<String> hiddenColumns();
    List<CellReferencesRange> cellReferencesRanges();
    void readWith(final XMLEventReader reader) throws XMLStreamException;

    class Default implements WorksheetFragments {
        private static final String SHEET_DATA = "sheetData";
        private static final String ROW = "row";
        private static final String COLS = "cols";
        private static final String COL = "col";
        private static final String MERGE_CELL = "mergeCell";
        private static final QName HIDDEN = new QName("hidden");
        private static final QName MIN = new QName("min");
        private static final QName MAX = new QName("max");
        private static final QName REF = new QName("ref");
        private static final QName ROW_INDEX = new QName("r");

        private final boolean cleanupAggressively;
        private final boolean translateHidden;
        private final boolean hidden;
        private final List<XMLEvent> events;
        private final Set<Integer> hiddenRows;
        private final Set<String> hiddenColumns;
        private final List<CellReferencesRange> cellReferencesRanges;

        Default(
            final boolean cleanupAggressively,
            final boolean translateHidden,
            final boolean hidden
        ) {
            this(
                cleanupAggressively,
                translateHidden,
                hidden,
                new LinkedList<>(),
                new LinkedHashSet<>(),
                new LinkedHashSet<>(),
                new LinkedList<>()
            );
        }

        Default(
            final boolean cleanupAggressively,
            final boolean translateHidden,
            final boolean hidden,
            final List<XMLEvent> events,
            final Set<Integer> hiddenRows,
            final Set<String> hiddenColumns,
            final List<CellReferencesRange> cellReferencesRanges
        ) {
            this.cleanupAggressively = cleanupAggressively;
            this.translateHidden = translateHidden;
            this.hidden = hidden;
            this.events = events;
            this.hiddenRows = hiddenRows;
            this.hiddenColumns = hiddenColumns;
            this.cellReferencesRanges = cellReferencesRanges;
        }

        @Override
        public List<XMLEvent> events() {
            return this.events;
        }

        @Override
        public Set<Integer> hiddenRows() {
            return this.hiddenRows;
        }

        @Override
        public Set<String> hiddenColumns() {
            return this.hiddenColumns;
        }

        @Override
        public List<CellReferencesRange> cellReferencesRanges() {
            return this.cellReferencesRanges;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            final List<XMLEvent> rowEvents = new LinkedList<>();
            final List<XMLEvent> cellEvents = new LinkedList<>();
            boolean inColumns = false;
            boolean inSheetData = false;
            boolean inRow = false;
            boolean inCell = false;
            boolean nonEmptyCellPresent = false;
            boolean cellChildElementPresent = false;
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isStartElement()) {
                    final StartElement se = e.asStartElement();
                    if (SHEET_DATA.equals(se.getName().getLocalPart())) {
                        inSheetData = true;
                        this.events.add(se);
                    } else if (inSheetData && ROW.equals(se.getName().getLocalPart())) {
                        inRow = true;
                        if (hidden(se)) {
                            this.hiddenRows.add(
                                Integer.parseUnsignedInt(se.getAttributeByName(ROW_INDEX).getValue())
                            );
                        }
                        rowEvents.add(se);
                    } else if (inRow && Cell.NAME.equals(se.getName().getLocalPart())) {
                        inCell = true;
                        cellEvents.add(se);
                    } else if (COLS.equals((se.getName().getLocalPart()))) {
                        inColumns = true;
                        this.events.add(se);
                    } else if (inColumns && COL.equals(se.getName().getLocalPart())) {
                        if (hidden(se)) {
                            // Column info blocks span one or more columns, which are referred to
                            // via 1-indexed min/max values.
                            this.hiddenColumns.addAll(columnNames(se));
                        }
                        this.events.add(se);
                    } else if (MERGE_CELL.equals(se.getName().getLocalPart())) {
                        this.cellReferencesRanges.add(new CellReferencesRange(se.getAttributeByName(REF).getValue()));
                        this.events.add(se);
                    } else if (inCell) {
                        cellChildElementPresent = true; // f || is || v
                        cellEvents.add(se);
                    } else {
                        this.events.add(se);
                    }
                } else if (e.isEndElement()) {
                    final EndElement ee = e.asEndElement();
                    if (inCell && Cell.NAME.equals(ee.getName().getLocalPart())) {
                        if (!this.cleanupAggressively || cellChildElementPresent) {
                            cellEvents.add(ee);
                            rowEvents.addAll(cellEvents);
                            cellChildElementPresent = false;
                            nonEmptyCellPresent = true;
                        }
                        cellEvents.clear();
                        inCell = false;
                    } else if (inRow && ROW.equals(ee.getName().getLocalPart())) {
                        if (!this.cleanupAggressively || nonEmptyCellPresent) {
                            rowEvents.add(ee);
                            this.events.addAll(rowEvents);
                            nonEmptyCellPresent = false;
                        }
                        rowEvents.clear();
                        inRow = false;
                    } else if (inSheetData && SHEET_DATA.equals(ee.getName().getLocalPart())) {
                        this.events.add(ee);
                        inSheetData = false;
                    } else if (inCell) {
                        cellEvents.add(ee);
                    } else if (inColumns && COLS.equals(ee.getName().getLocalPart())) {
                        this.events.add(ee);
                        inColumns = false;
                    } else {
                        this.events.add(ee);
                    }
                } else {
                    if (inCell) {
                        cellEvents.add(e);
                    } else if (inRow) {
                        rowEvents.add(e);
                    } else {
                        this.events.add(e);
                    }
                }
            }
        }

        private boolean hidden(final StartElement startElement) {
            return !this.translateHidden &&
                (this.hidden || XMLEventHelpers.getBooleanAttributeValue(startElement, HIDDEN, XMLEventHelpers.DEFAULT_BOOLEAN_ATTRIBUTE_FALSE_VALUE));
        }

        /**
         * Convert the min and max attributes of a &lt;col&gt; element into a list
         * of column names.  For example, "min=2; max=2" => [ "B" ].
         *
         * @param startElement
         * @return
         */
        private List<String> columnNames(final StartElement startElement) {
            try {
                List<String> names = new ArrayList<>();
                int min = Integer.parseUnsignedInt(startElement.getAttributeByName(MIN).getValue());
                int max = Integer.parseUnsignedInt(startElement.getAttributeByName(MAX).getValue());
                for (int i = min; i <= max; i++) {
                    names.add(indexToColumnName(i));
                }
                return names;
            } catch (NumberFormatException | NullPointerException e) {
                throw new OkapiBadFilterInputException("Invalid <col> element", e);
            }
        }

        private static String indexToColumnName(int index) {
            final StringBuilder sb = new StringBuilder();
            while (index > 0) {
                int modulo = (index - 1) % 26;
                sb.insert(0, (char) (65 + modulo));
                index = (index - modulo) / 26;
            }
            return sb.toString();
        }
    }
}

/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.events.XMLEvent;
import java.util.List;

final class RunPropertiesPairWithDetectedRunFonts implements XMLEvents {
    private final RunProperties direct;
    private final RunProperties combined;
    private final RunFonts detectedRunFonts;

    RunPropertiesPairWithDetectedRunFonts(
        final RunProperties direct,
        final RunProperties combined,
        final RunFonts detectedRunFonts
    ) {
        this.direct = direct;
        this.combined = combined;
        this.detectedRunFonts = detectedRunFonts;
    }

    RunProperties direct() {
        return this.direct;
    }

    RunProperties combined() {
        return this.combined;
    }

    RunFonts detectedRunFonts() {
        return this.detectedRunFonts;
    }

    @Override
    public List<XMLEvent> getEvents() {
        return this.direct.getEvents();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" + this.direct + ", " + this.combined + ", " + this.detectedRunFonts + ")";
    }
}

/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

interface NumberFormat {
    String NAME = "numFmt";
    int id();
    boolean dateTimeBased();
    DateTimeFormatter dateTimeFormatter();
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    Markup asMarkup();

    final class BuiltIn implements NumberFormat {
        static final Set<Integer> ids = new HashSet<>();
        static {
            // All languages
            ids.addAll(IntStream.rangeClosed(0, 4).boxed().collect(Collectors.toSet()));
            ids.addAll(IntStream.rangeClosed(9, 22).boxed().collect(Collectors.toSet()));
            ids.addAll(IntStream.rangeClosed(37, 40).boxed().collect(Collectors.toSet()));
            ids.addAll(IntStream.rangeClosed(45, 49).boxed().collect(Collectors.toSet()));
            // CHT and CHS, JPN and KOR
            ids.addAll(IntStream.rangeClosed(27, 36).boxed().collect(Collectors.toSet()));
            ids.addAll(IntStream.rangeClosed(50, 58).boxed().collect(Collectors.toSet()));
            // THA
            ids.addAll(IntStream.rangeClosed(59, 81).boxed().collect(Collectors.toSet()));
        }
        private static final Map<Integer, String> dateOrTimeCodes = new HashMap<>();
        static {
            // for all languages
            dateOrTimeCodes.put(14, "MM-dd-yy"); // mm-dd-yy
            dateOrTimeCodes.put(15, "d-MM-yy"); // d-mm-yy
            dateOrTimeCodes.put(16, "d-MM"); // d-mmm
            dateOrTimeCodes.put(17, "MM-yy"); // mmm-yy
            dateOrTimeCodes.put(18, "H:mm a"); // h:mm AM/PM
            dateOrTimeCodes.put(19, "H:mm:ss a"); // h:mm:ss AM/PM
            dateOrTimeCodes.put(20, "H:mm"); // h:mm
            dateOrTimeCodes.put(21, "H:mm:ss"); // h:mm:ss
            dateOrTimeCodes.put(22, "M/d/yy H:mm"); // m/d/yy h:mm
            dateOrTimeCodes.put(45, "mm:ss"); // mm:ss
            dateOrTimeCodes.put(47, "mmss.S"); // mmss.0
        }
        static final int GENERAL_ID = 0;
        static final String GENERAL_VALUE = "";
        static final BuiltIn GENERAL = new BuiltIn(GENERAL_ID);
        private final int id;

        BuiltIn(final int id) {
            this.id = id;
        }

        @Override
        public int id() {
            return this.id;
        }

        @Override
        public boolean dateTimeBased() {
            return BuiltIn.dateOrTimeCodes.containsKey(this.id);
        }

        @Override
        public DateTimeFormatter dateTimeFormatter() {
            if (BuiltIn.dateOrTimeCodes.containsKey(this.id)) {
                return DateTimeFormatter.ofPattern(BuiltIn.dateOrTimeCodes.get(this.id));
            }
            return DateTimeFormatter.ofPattern(GENERAL_VALUE);
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
        }

        @Override
        public Markup asMarkup() {
            return new Markup.Empty();
        }
    }

    final class Explicit implements NumberFormat {
        private static final String EMPTY = BuiltIn.GENERAL_VALUE;
        private static final Set<String> dateCodes = new HashSet<>(
            Arrays.asList(
                "d", "m", "yy"
            )
        );
        private static final Set<String> timeCodes = new HashSet<>(
            Arrays.asList(
                "h", "m", "s"
            )
        );
        private static final String NUM_FMT_ID = "numFmtId";
        private static final String FORMAT_CODE = "formatCode";
        private final StartElement startElement;
        private int id;
        private String value;
        private EndElement endElement;
        private boolean startElementAttributesRead;

        Explicit(final StartElement startElement) {
            this.startElement = startElement;
        }

        @Override
        public int id() {
            if (!this.startElementAttributesRead) {
                readStartElementAttributes();
            }
            return this.id;
        }

        @Override
        public boolean dateTimeBased() {
            if (!this.startElementAttributesRead) {
                readStartElementAttributes();
            }
            return Explicit.dateCodes.stream().anyMatch(c -> this.value.contains(c))
                || Explicit.timeCodes.stream().anyMatch(c -> this.value.contains(c));
        }

        private void readStartElementAttributes() {
            final Iterator iterator = this.startElement.getAttributes();
            while (iterator.hasNext()) {
                final Attribute a = (Attribute) iterator.next();
                switch (a.getName().getLocalPart()) {
                    case NUM_FMT_ID:
                        this.id = Integer.parseUnsignedInt(a.getValue());
                        break;
                    case FORMAT_CODE:
                        this.value = a.getValue();
                        break;
                }
            }
            this.startElementAttributesRead = true;
        }

        /**
         * Obtains a date-time formatter from this number format.
         * Todo: transform the available date and time codes to Java equivalents.
         * For more details on number formats please refer to the following resource -
         * https://docs.microsoft.com/en-us/dotnet/api/documentformat.openxml.spreadsheet.numberingformats?view=openxml-2.8.1
         * @return If a value belongs to one of supported built-in date or time codes, then
         *          a corresponding date-time formatter is returned,
         *          otherwise, an ISO-8601 date-time formatter is returned
         */
        @Override
        public DateTimeFormatter dateTimeFormatter() {
            final boolean dateAvailable = Explicit.dateCodes.stream().anyMatch(c -> this.value.contains(c));
            final boolean timeAvailable = Explicit.timeCodes.stream().anyMatch(c -> this.value.contains(c));
            if (dateAvailable && timeAvailable) {
                return DateTimeFormatter.ISO_DATE_TIME;
            }
            if (dateAvailable) {
                return DateTimeFormatter.ISO_DATE;
            }
            if (timeAvailable) {
                return DateTimeFormatter.ISO_TIME;
            }
            return DateTimeFormatter.ofPattern(EMPTY);
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
            }
        }

        @Override
        public Markup asMarkup() {
            if (null == this.endElement) {
                throw new IllegalStateException("The end element is not available");
            }
            final Markup markup = new Markup.General(new LinkedList<>());
            markup.addComponent(
                new MarkupComponent.General(
                    Arrays.asList(this.startElement, this.endElement)
                )
            );
            return markup;
        }
    }
}

/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

interface Cells {
    void add(final Cell cell);
    void add(final Cells cells);
    Cells of(final CellType type);
    ListIterator<Cell> iterator();
    boolean worksheetStartsAt(final ListIterator<Cell> iterator);
    boolean rowStartsAt(final ListIterator<Cell> iterator);
    Markup asMarkup();

    class Default implements Cells {
        private final boolean extractCopied;
        private final XMLEventFactory eventFactory;
        private final List<Cell> items;
        private final Set<String> formerValues;
        private int sharedStringIndex;

        Default(final boolean extractCopied, final XMLEventFactory eventFactory) {
            this(extractCopied, eventFactory, new ArrayList<>(), new HashSet<>());
        }

        Default(
            final boolean extractCopied,
            final XMLEventFactory eventFactory,
            final List<Cell> items,
            final Set<String> formerValues
        ) {
            this.extractCopied = extractCopied;
            this.eventFactory = eventFactory;
            this.items = items;
            this.formerValues = formerValues;
        }

        @Override
        public void add(final Cell cell) {
            if (this.extractCopied) {
                addAndUpdate(cell);
            } else {
                if (CellType.SHARED_STRING == cell.type() && !cell.excluded()) {
                    final String fs = cell.value().asFormattedString();
                    if (!this.formerValues.contains(fs)) {
                        addAndUpdate(cell);
                        this.formerValues.add(fs);
                    }
                } else {
                    addAndUpdate(cell);
                }
            }
        }

        private void addAndUpdate(final Cell cell) {
            this.items.add(cell);
            if (CellType.SHARED_STRING == cell.type() || CellType.INLINE_STRING == cell.type()) {
                cell.value().update(this.eventFactory.createCharacters(String.valueOf(this.sharedStringIndex)));
                this.sharedStringIndex++;
            }
        }

        @Override
        public void add(final Cells cells) {
            final Iterator<Cell> iterator = cells.iterator();
            while (iterator.hasNext()) {
                add(iterator.next());
            }
        }

        @Override
        public Cells of(final CellType type) {
            return new Default(
                this.extractCopied,
                this.eventFactory,
                this.items.stream()
                    .filter(c -> c.type() == type)
                    .collect(Collectors.toList()),
                new HashSet<>(this.formerValues)
            );
        }

        @Override
        public ListIterator<Cell> iterator() {
            return this.items.listIterator();
        }

        @Override
        public boolean worksheetStartsAt(final ListIterator<Cell> iterator) {
            if (!iterator.hasPrevious()) {
                // at the start of the list
                return true;
            }
            final Cell current = iterator.previous();
            if (!iterator.hasPrevious()) {
                // at the start of the list
                iterator.next(); // restore the position
                return true;
            }
            final Cell previous = iterator.previous();
            iterator.next(); // restore the position
            iterator.next();
            return !previous.worksheetName().equals(current.worksheetName());
        }

        @Override
        public boolean rowStartsAt(final ListIterator<Cell> iterator) {
            if (!iterator.hasPrevious()) {
                // at the start of the list
                return true;
            }
            final Cell current = iterator.previous();
            if (!iterator.hasPrevious()) {
                // at the start of the list
                iterator.next(); // restore the position
                return true;
            }
            final Cell previous = iterator.previous();
            iterator.next(); // restore the position
            iterator.next();
            return previous.cellReferencesRange().first().row() != current.cellReferencesRange().first().row();
        }

        @Override
        public Markup asMarkup() {
            final Markup m = new Markup.General(new ArrayList<>(this.items.size()));
            this.items.forEach(cell -> m.add(cell.asMarkup()));
            return m;
        }
    }

    class Sorted implements Cells {
        private final boolean copyForExtraction;
        private final SourceAndTargetColumns sourceAndTargetColumns;
        private final SortedMap<String, Cell> items;

        Sorted(final boolean copyForExtraction, final SourceAndTargetColumns sourceAndTargetColumns) {
            this(
                copyForExtraction,
                sourceAndTargetColumns,
                new TreeMap<>(new StringsComparator())
            );
        }

        Sorted(
            final boolean copyForExtraction,
            final SourceAndTargetColumns sourceAndTargetColumns,
            final SortedMap<String, Cell> items
        ) {
            this.copyForExtraction = copyForExtraction;
            this.sourceAndTargetColumns = sourceAndTargetColumns;
            this.items = items;
        }

        @Override
        public void add(final Cell cell) {
            final String column = cell.cellReferencesRange().first().column();
            this.items.put(column, cell);
        }

        @Override
        public void add(final Cells cells) {
            final Iterator<Cell> iterator = cells.iterator();
            while (iterator.hasNext()) {
                add(iterator.next());
            }
        }

        @Override
        public Cells of(final CellType type) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ListIterator<Cell> iterator() {
            alignWithSourceAndTargetColumns();
            return new ArrayList<>(this.items.values()).listIterator();
        }

        private void alignWithSourceAndTargetColumns() {
            this.sourceAndTargetColumns.source().forEach(sc -> {
                final String tc = this.sourceAndTargetColumns.targetFor(sc);
                if (this.items.containsKey(sc)) {
                    final Cell cell = this.items.get(sc).copiedWithAdjusted(tc);
                    this.items.put(tc, cell);
//                    if (!this.copyForExtraction) {
//                        cell.value().updateFormerCharacters();
//                    }
                } else {
                    // source does not exist
                    this.items.remove(tc);
                }
            });
        }

        @Override
        public boolean worksheetStartsAt(final ListIterator<Cell> iterator) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean rowStartsAt(final ListIterator<Cell> iterator) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Markup asMarkup() {
            final Markup m = new Markup.General(new ArrayList<>(this.items.size()));
            this.items.values().forEach(cell -> m.add(cell.asMarkup()));
            return m;
        }
    }
}

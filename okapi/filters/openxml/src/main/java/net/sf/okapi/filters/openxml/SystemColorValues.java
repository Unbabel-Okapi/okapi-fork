/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.util.Arrays;
import java.util.List;

interface SystemColorValues {
    Color.Value valueFor(final String string);

    final class Default implements SystemColorValues {
        private static final String DEFAULT = "";
        private final List<Color.Value> values;

        Default() {
            this(
                Arrays.asList(
                    // todo: provide specific values
                    new Color.System.Value("scrollBar", Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value("background", Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value("activeCaption", Color.System.Value.DEFAULT_FOREGROUND),
                    new Color.System.Value("inactiveCaption", Color.System.Value.DEFAULT_FOREGROUND),
                    new Color.System.Value("menu", Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value(Color.System.DEFAULT_BACKGROUND_NAME, Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value("windowFrame", Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value("menuText", Color.System.Value.DEFAULT_FOREGROUND),
                    new Color.System.Value(Color.System.DEFAULT_FOREGROUND_NAME, Color.System.Value.DEFAULT_FOREGROUND),
                    new Color.System.Value("captionText", Color.System.Value.DEFAULT_FOREGROUND),
                    new Color.System.Value("activeBorder", Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value("inactiveBorder", Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value("appWorkspace", Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value("highlight", Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value("highlightText", Color.System.Value.DEFAULT_FOREGROUND),
                    new Color.System.Value("btnFace", Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value("btnShadow", Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value("grayText", Color.System.Value.DEFAULT_FOREGROUND),
                    new Color.System.Value("btnText", Color.System.Value.DEFAULT_FOREGROUND),
                    new Color.System.Value("inactiveCaptionText", Color.System.Value.DEFAULT_FOREGROUND),
                    new Color.System.Value("btnHighlight", Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value("3dDkShadow", Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value("3dLight", Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value("infoText", Color.System.Value.DEFAULT_FOREGROUND),
                    new Color.System.Value("infoBk", Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value("hotLight", Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value("gradientActiveCaption", Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value("gradientInactiveCaption", Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value("menuHighlight", Color.System.Value.DEFAULT_BACKGROUND),
                    new Color.System.Value("menuBar", Color.System.Value.DEFAULT_BACKGROUND)
                )
            );
        }

        Default(final List<Color.Value> values) {
            this.values = values;
        }

        @Override
        public Color.Value valueFor(final String string) {
            return this.values.stream()
                .filter(v -> v.asRgb().equals(string) || v.asInternalName().equals(string))
                .findFirst()
                .orElse(new Color.Value.Empty());
        }
    }
}

package net.sf.okapi.filters.cascadingfilter;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filters.ThreadSafeFilterConfigurationMapper;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.html.HtmlFilter;
import net.sf.okapi.filters.json.JSONFilter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RunWith(JUnit4.class)

public class CascadingFilterTest {
    private CascadingFilter cascadingFilter;
    private FilterParameters maximal;
    private FilterParameters minimal;
    private ThreadSafeFilterConfigurationMapper mapper;

    static Map<String, FilterConfiguration> loadConfigs() {
        var builder = new ThreadSafeFilterConfigurationMapper.ConfigBuilder();
        builder.addConfigurations(HtmlFilter.class);
        builder.addConfigurations(JSONFilter.class);
        return builder.build();
    }

    @Before
    public void setUp() {
        maximal = new FilterParameters(
                "okf_json",
                List.of("okf_html"),
                new JSONFilter().getParameters().toString(),
                Collections.singletonList(new HtmlFilter().getParameters().toString()));
        minimal = new FilterParameters(
                "okf_json",
                null,
                null,
                null);
        mapper = new ThreadSafeFilterConfigurationMapper(CascadingFilterTest::loadConfigs);
    }

    @Test
    public void extractMinimalWithStream() {
        cascadingFilter = CascadingFilter.createCascadingFilter(minimal, mapper);
        final String original = "/test.json";
        InputStream originalDocument = getClass().getResourceAsStream(original);
        assert originalDocument != null;
        cascadingFilter.open(new RawDocument(originalDocument, "UTF-8",
                LocaleId.fromString("en-US"),
                LocaleId.fromString("es-ES")));
        List<Event> tus = cascadingFilter.stream().filter(Event::isTextUnit).collect(Collectors.toList());
        Assert.assertNotNull(tus);
        Assert.assertEquals(2, tus.size());

        ITextUnit tu = tus.get(0).getTextUnit();
        compareMinimalTu1(tu);
        tu = tus.get(1).getTextUnit();
        compareMinimalTu2(tu);

        cascadingFilter.close();
    }

    @Test
    public void extractMinimalNormally() {
        cascadingFilter = CascadingFilter.createCascadingFilter(minimal, mapper);
        final String original = "/test.json";
        InputStream originalDocument = getClass().getResourceAsStream(original);
        assert originalDocument != null;
        List<Event> tus = FilterTestDriver.getTextUnitEvents(cascadingFilter,
                new RawDocument(originalDocument, "UTF-8",
                        LocaleId.fromString("en-US"),
                        LocaleId.fromString("es-ES")));

        Assert.assertNotNull(tus);
        Assert.assertEquals(2, tus.size());

        ITextUnit tu = tus.get(0).getTextUnit();
        compareMinimalTu1(tu);
        tu = tus.get(1).getTextUnit();
        compareMinimalTu2(tu);

        cascadingFilter.close();
    }

    @Test
    public void extractWithStream() {
        cascadingFilter = CascadingFilter.createCascadingFilter(maximal, mapper);
        final String original = "/test.json";
        InputStream originalDocument = getClass().getResourceAsStream(original);
        assert originalDocument != null;
        cascadingFilter.open(new RawDocument(originalDocument, "UTF-8",
                LocaleId.fromString("en-US"),
                LocaleId.fromString("es-ES")));
        List<Event> tus = cascadingFilter.stream().filter(Event::isTextUnit).collect(Collectors.toList());
        Assert.assertNotNull(tus);
        Assert.assertEquals(3, tus.size());

        ITextUnit tu = tus.get(0).getTextUnit();
        compareMaximalTu1(tu);
        tu = tus.get(1).getTextUnit();
        compareMaximalTu2(tu);
        tu = tus.get(2).getTextUnit();
        compareMaximalTu3(tu);

        cascadingFilter.close();
    }

    @Test
    public void extractNormally() {
        final String original = "/test.json";
        InputStream originalDocument = getClass().getResourceAsStream(original);
        assert originalDocument != null;

        cascadingFilter = CascadingFilter.createCascadingFilter(maximal, mapper);
        List<Event> tus = FilterTestDriver.getTextUnitEvents(cascadingFilter,
                new RawDocument(originalDocument, "UTF-8",
                LocaleId.fromString("en-US"),
                LocaleId.fromString("es-ES")));

        Assert.assertNotNull(tus);
        Assert.assertEquals(3, tus.size());

        ITextUnit tu = tus.get(0).getTextUnit();
        compareMaximalTu1(tu);
        tu = tus.get(1).getTextUnit();
        compareMaximalTu2(tu);
        tu = tus.get(2).getTextUnit();
        compareMaximalTu3(tu);

        cascadingFilter.close();
    }

    private void compareMaximalTu1(ITextUnit tu) {
        Assert.assertEquals("tu1_sf1_tu1", tu.getId());
        Assert.assertEquals("one_1", tu.getName());
        Assert.assertEquals("text/html", tu.getMimeType());
        Assert.assertEquals("HTML <b><font>transitional</font></b> \"<mode\".", tu.getSource().toString());
    }

    private void compareMaximalTu2(ITextUnit tu) {
        Assert.assertEquals("tu1_sf1_tu2", tu.getId());
        Assert.assertEquals("one_2", tu.getName());
        Assert.assertEquals("text/html", tu.getMimeType());
        Assert.assertEquals("More text.", tu.getSource().toString());
    }

    private void compareMaximalTu3(ITextUnit tu) {
        Assert.assertEquals("tu2_sf2_tu1", tu.getId());
        Assert.assertEquals("two_1", tu.getName());
        Assert.assertEquals("text/html", tu.getMimeType());
        Assert.assertEquals("<i><u>emphasis&</u></i>", tu.getSource().toString());
    }

    private void compareMinimalTu1(ITextUnit tu) {
        Assert.assertEquals("tu1", tu.getId());
        Assert.assertEquals("one", tu.getName());
        Assert.assertEquals("application/json", tu.getMimeType());
        Assert.assertEquals("HTML <b><font>transitional</font></b> \"&lt;mode\".<p> More text.", tu.getSource().toString());
    }

    private void compareMinimalTu2(ITextUnit tu) {
        Assert.assertEquals("tu2", tu.getId());
        Assert.assertEquals("two", tu.getName());
        Assert.assertEquals("application/json", tu.getMimeType());
        Assert.assertEquals("<i><u>emphasis&amp;</u></i>", tu.getSource().toString());
    }
}

package net.sf.okapi.filters.markdown.parser;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class HeaderAnchorGenerator {

	private final Map<String, Integer> nextIdSuffix = new HashMap<>();

	/**
	 * Convert header text to an anchor id, following the steps described in
	 * https://docs.gitlab.com/ee/user/markdown.html#header-ids-and-links:
	 *
	 * 1. All text is converted to lowercase. 2. All non-word text (such as
	 * punctuation or HTML) is removed. (Note: our implementation treats underscore
	 * as a "word" character.) 3. All spaces are converted to hyphens. 4. Two or
	 * more hyphens in a row are converted to one. 5. If a header with the same ID
	 * has already been generated, a unique incrementing number is appended,
	 * starting at 1.
	 *
	 * @return the generated anchor id, not including the surrounding markup
	 *         (braces)
	 */
	public String generateAnchorText(String text) {
		text = text.toLowerCase();
		text = text.replaceAll("[^\\p{L}\\p{Digit}_ ]", " "); // replace all non-letters other than space
		text = text.trim(); // deal with trailing space
		text = text.replaceAll(" ", "-"); // spaces become hyphens
		text = text.replaceAll("-+", "-"); // normalize hyphens

		// Add a distinguishing suffix if this id is non-unique
		Optional<Integer> maybeSuffix = Optional.ofNullable(nextIdSuffix.get(text));
		if (maybeSuffix.isPresent()) {
			nextIdSuffix.put(text, maybeSuffix.get() + 1);
			text = text + "-" + maybeSuffix.get();
		} else {
			nextIdSuffix.put(text, 1);
		}
		return text;
	}
}

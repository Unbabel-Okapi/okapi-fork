package net.sf.okapi.filters.messageformat;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;
import net.sf.okapi.common.filters.SubFilterSkeletonWriter;
import net.sf.okapi.common.resource.*;

public class MessageFormatSkeletonWriter extends SubFilterSkeletonWriter {
    private final LocaleId targetLocale;

    public MessageFormatSkeletonWriter(StartSubfilter resource, LocaleId targetLocale) {
        super(resource);
        this.targetLocale = targetLocale;
    }

    @Override
    public void close() {
        super.close();
    }

    @Override
    public String processStartDocument(LocaleId outputLocale, String outputEncoding, EncoderManager encoderManager, StartDocument resource) {
        return super.processStartDocument(outputLocale, outputEncoding, encoderManager, resource);
    }

    @Override
    public String processEndDocument(Ending resource) {
        try(MessageFormatParser p = new MessageFormatParser()) {
            p.parse(super.getOutput());
            super.updateTargetOutputs(p.toString());
            return super.processEndDocument(resource);
        } catch (Exception e) {
            throw new OkapiBadFilterInputException("Error validating translated message string", e);
        }
    }
}

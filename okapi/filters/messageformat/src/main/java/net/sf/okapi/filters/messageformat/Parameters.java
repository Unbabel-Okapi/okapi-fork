/*
 * ====================================================================
 *   Copyright (C) $time.year by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =====================================================================
 */

package net.sf.okapi.filters.messageformat;

import net.sf.okapi.common.ISimplifierRulesParameters;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.filters.InlineCodeFinder;

public class Parameters extends StringParameters implements ISimplifierRulesParameters {
    public static final String ADDPLURALFORMS = "addPluralForms";
    public static final String NORMALIZE = "normalize";
    public static final String PRETTY_PRINT = "prettyPrint";
    private static final String CODEFINDERRULES = "codeFinderRules";
    private static final String USECODEFINDER = "useCodeFinder";

    // Initialized in reset()
    private InlineCodeFinder codeFinder;


    public Parameters() {
        super();
    }

    @Override
    public void reset() {
        super.reset();
        setAddPluralForms(false);
        setNormalize(false);
        setPrettyPrint(false);
        setSimplifierRules(null);
        setUseCodeFinder(false);
        codeFinder = new InlineCodeFinder();
        codeFinder.setSample("%s, %d, {1}, \\n, \\r, \\t, {{var}} etc.");
        codeFinder.setUseAllRulesWhenTesting(true);
        codeFinder.addRule("</?([A-Z0-9a-z]*)\\b[^>]*>");
        codeFinder.addRule("%(([-0+#]?)[-0+#]?)((\\d\\$)?)(([\\d\\*]*)(\\.[\\d\\*]*)?)[dioxXucsfeEgGpnYyBbHhSMmAZ]");
        codeFinder.addRule("(\\\\r\\\\n)|\\\\a|\\\\b|\\\\f|\\\\n|\\\\r|\\\\t|\\\\v");
        codeFinder.addRule("\\{\\{\\w.*?\\}\\}");
    }

    public boolean getUseCodeFinder() {
        return getBoolean(USECODEFINDER);
    }

    public void setUseCodeFinder(boolean useCodeFinder) {
        setBoolean(USECODEFINDER, useCodeFinder);
    }

    public InlineCodeFinder getCodeFinder() {
        return codeFinder;
    }

    public boolean isAddPluralForms() {
        return getBoolean(ADDPLURALFORMS);
    }

    public void setAddPluralForms(boolean addPluralForms) {
        setBoolean(ADDPLURALFORMS, addPluralForms);
    }

    public boolean isNormalize() { return getBoolean(NORMALIZE); }

    public void setNormalize(boolean normalize) {
        setBoolean(NORMALIZE, normalize);
    }

    public boolean isPrettyPrint() { return getBoolean(PRETTY_PRINT); }

    public void setPrettyPrint(boolean prettyPrint) {
        setBoolean(PRETTY_PRINT, prettyPrint);
    }

    @Override
    public String getSimplifierRules() {
        return getString(SIMPLIFIERRULES);
    }

    @Override
    public void setSimplifierRules(String rules) {
        setString(SIMPLIFIERRULES, rules);
    }

    @Override
    public void fromString(String data) {
        super.fromString(data);
        codeFinder.fromString(buffer.getGroup(CODEFINDERRULES, ""));
    }

    @Override
    public String toString() {
        buffer.setGroup(CODEFINDERRULES, codeFinder.toString());
        return super.toString();
    }
}

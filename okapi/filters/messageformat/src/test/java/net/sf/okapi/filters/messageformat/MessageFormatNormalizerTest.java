package net.sf.okapi.filters.messageformat;

import com.ibm.icu.text.MessageFormat;
import com.ibm.icu.util.ULocale;
import net.sf.okapi.common.StringUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class MessageFormatNormalizerTest {

	@Test
	public void testNormalize() throws Exception {
		// Alternating strings for testing. Even: input, odd: expected (normalized).
		final String[] tests = {
				null,
				"",
				// Empty
				"",
				"",
				// No selection, no arguments
				"Hello world!",
				"Hello world!",
				// Argument in various locations, no selectors
				"{name}", // stand-alone
				"{name}",
				"Hello, {name}", // at the end
				"Hello, {name}",
				"{name} beware!", // at the beginning
				"{name} beware!",
				"Hello '{name}', beware!", // in the middle
				"Hello '{name}', beware!",
				// One selector, wrapping full messages, already normalized
				"{gender,select,female{she}male{he}other{they}}",
				"{gender, select, female {she} male {he} other {they}}",
				"{count,plural,=1{one}other{more}}",
				"{count, plural, =1 {one} other {more}}",
				// One selector, various locations
				"{gender,select,female{she's}male{he's}other{they're}} cool!", // at the beginning
				"{gender,select,female{she's cool!}male{he's cool!}other{they're cool!}}",
				"Sais {gender,select,female{she}male{he}other{they}}", // at the end
				"{gender,select,female{Sais she}male{Sais he}other{Sais they}}",
				"This is {gender,select,female{her}male{his}other{their}} house.", // in the middle
				"{gender,select,female{This is her house.}male{This is his house.}other{This is their house.}}",
		    	// Plural with offset
		    	"{name} and {count,plural,offset:1"
		    	+   " =1{one more friend}"
		    	+   "other{# more friends}} went to the zoo.",
		    	"{count,plural,offset:1 "
		    	+   "=1{{name} and one more friend went to the zoo.}"
		    	+   "other{{name} and # more friends went to the zoo.}"
		    	+ "}",
				// Two selectors, independent: ...{gender...}...{count...#}...
		    	"This is {gender,select,female{her}male{his}other{their}} home. And has {count,plural,=1{a door}other{# doors}}!",
		    	"{gender,select,"
		    	+ "female{{count,plural,"
		    	+   "=1{This is her home. And has a door!}"
		    	+   "other{This is her home. And has # doors!}"
		    	+ "}}"
		    	+ "male{{count,plural,"
		    	+   "=1{This is his home. And has a door!}"
		    	+   "other{This is his home. And has # doors!}"
		    	+ "}}"
		    	+ "other{{count,plural,"
		    	+   "=1{This is their home. And has a door!}"
		    	+   "other{This is their home. And has # doors!}"
		    	+ "}}}",
				// Two selectors, independent: ...{gender...}...{count...{count}}...
		    	"This is {gender,select,female{her}male{his}other{their}} home. And has {count,plural,=1{a door}other{{count} doors}}!",
		    	"{gender,select,"
		    	+ "female{{count,plural,"
		    	+   "=1{This is her home. And has a door!}"
		    	+   "other{This is her home. And has {count} doors!}"
		    	+ "}}"
		    	+ "male{{count,plural,"
		    	+   "=1{This is his home. And has a door!}"
		    	+   "other{This is his home. And has {count} doors!}"
		    	+ "}}"
		    	+ "other{{count,plural,"
		    	+   "=1{This is their home. And has a door!}"
		    	+   "other{This is their home. And has {count} doors!}"
		    	+ "}}}",
				// Two selectors, nested: ...{gender...{count...}...}...
		    	"I saw {gender,select,"
		    	+ "female{her {count,plural,=1{friend}other{# friends}} in the}"
		    	+ "male{his {count,plural,=1{friend}other{# friends}} in the}"
		    	+ "other{their {count,plural,=1{friend}other{# friends}} in the}} park.",
		    	"{gender,select,"
		    	+ "female{{count,plural,"
		    	+   "=1{I saw her friend in the park.}"
		    	+   "other{I saw her # friends in the park.}}}"
		    	+ "male{{count,plural,"
		    	+   "=1{I saw his friend in the park.}"
		    	+   "other{I saw his # friends in the park.}}}"
		    	+ "other{{count,plural,"
		    	+   "=1{I saw their friend in the park.}"
		    	+   "other{I saw their # friends in the park.}}}}",
		};
		for (int i = 0; i < tests.length; i += 2) {
			MessageFormatParser p = new MessageFormatParser(tests[i]);
			p.normalize();
			final String result = p.toString();
			final String expected = tests[i + 1];
	        assertEquals(StringUtil.removeWhiteSpace(expected), StringUtil.removeWhiteSpace(result));
		}

		final Map<String, Object> arguments = new HashMap<>();
		arguments.put("name", "John");
		final String[] genders = { "female", "male", "something" };
		final Integer[] counts = { 1, 3 };
		final ULocale locale = new ULocale("ro");

		for (int i = 2; i < tests.length; i += 2) {
			MessageFormatParser p = new MessageFormatParser(tests[i]);
			p.normalize();
			final String result = p.toString();
			final String expected = tests[i + 1];
			final MessageFormat mfResult = new MessageFormat(result, locale);
			final MessageFormat mfExpected = new MessageFormat(expected, locale);
			for (final String gender : genders) {
				arguments.put("gender", gender);
				for (Integer count : counts) {
					arguments.put("count", count);
					assertEquals(mfExpected.format(arguments), mfResult.format(arguments));
				}
			}
		}
    }
}

/*
 * ====================================================================
 *   Copyright (C) $time.year by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =====================================================================
 */

package net.sf.okapi.filters.messageformat;

import net.sf.okapi.common.LocaleId;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PluralRulesDiffTest {
  @Before
  public void setUp() {}

  // JUnit tests
  @Test
  public void testRomanian() {
    PluralDiff diff = PluralRulesUtil.diffPluralRules(LocaleId.ENGLISH, LocaleId.fromBCP47(("ro")), com.ibm.icu.text.PluralRules.PluralType.CARDINAL);
    Assert.assertTrue(diff.getAdded().contains("few"));
    Assert.assertTrue(diff.getRemoved().isEmpty());
  }

  @Test 
  public void testSpanish() {
    PluralDiff diff = PluralRulesUtil.diffPluralRules(LocaleId.ENGLISH, LocaleId.fromBCP47("es"), com.ibm.icu.text.PluralRules.PluralType.CARDINAL);
    Assert.assertTrue(diff.getAdded().contains("many"));
    Assert.assertTrue(diff.getRemoved().isEmpty());
  }  

  @Test
  public void testArabic() {
    PluralDiff diff = PluralRulesUtil.diffPluralRules(LocaleId.ENGLISH, LocaleId.fromBCP47("ar"), com.ibm.icu.text.PluralRules.PluralType.CARDINAL);
    Assert.assertTrue(diff.getAdded().contains("few"));
    Assert.assertTrue(diff.getAdded().contains("zero"));
    Assert.assertTrue(diff.getAdded().contains("many"));
    Assert.assertTrue(diff.getAdded().contains("two"));
    Assert.assertTrue(diff.getRemoved().isEmpty());
  }

  @Test
  public void testRussian() {
    PluralDiff diff = PluralRulesUtil.diffPluralRules(LocaleId.ENGLISH, LocaleId.fromBCP47("ru"), com.ibm.icu.text.PluralRules.PluralType.CARDINAL);
    Assert.assertTrue(diff.getAdded().contains("few"));
    Assert.assertTrue(diff.getAdded().contains("many"));
    Assert.assertTrue(diff.getRemoved().isEmpty());
  }

  @Test
  public void testGerman() {
    PluralDiff diff = PluralRulesUtil.diffPluralRules(LocaleId.ENGLISH, LocaleId.fromBCP47("de"), com.ibm.icu.text.PluralRules.PluralType.CARDINAL);
    Assert.assertTrue(diff.getAdded().isEmpty());
    Assert.assertTrue(diff.getRemoved().isEmpty());
  }
}

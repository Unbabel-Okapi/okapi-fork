/*===========================================================================
  Copyright (C) 2008-2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.subtitles;

import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.*;

/**
 * VTT Filter parameters
 * 
 * @version 0.1, 07.06.23
 */

public class SubtitleParameters extends StringParameters implements IEditorDescriptionProvider {

	private static final String TIMEFORMAT = "timeFormat";
	private static final String MAXLINESPERCAPTION = "maxLinesPerCaption";
	private static final String MAXCHARSPERLINE = "maxCharsPerLine";
	private static final String CJKCHARSPERLINE = "cjkCharsPerLine";
	private static final String KEEPTIMECODES = "keepTimecodes";
	private static final String SPLITWORDS = "splitWords"; // split words so that they don't go over the character limit.

	public SubtitleParameters() {
		super();
	}

	public String getTimeFormat() { return getString(TIMEFORMAT); }

	public void setTimeFormat(String timeFormat) { setString(TIMEFORMAT, timeFormat); }

	public int getMaxLinesPerCaption() { return getInteger(MAXLINESPERCAPTION); }

	public void setMaxLinesPerCaption(int maxLinesPerCaption) { setInteger(MAXLINESPERCAPTION, maxLinesPerCaption); }

	public int getMaxCharsPerLine() { return getInteger(MAXCHARSPERLINE); }

	public void setMaxCharsPerLine(int maxCharsPerLine) { setInteger(MAXCHARSPERLINE, maxCharsPerLine); }

	public int getCjkCharsPerLine() { return getInteger(CJKCHARSPERLINE); }

	public void setCjkCharsPerLine(int cjkCharsPerLine) { setInteger(CJKCHARSPERLINE, cjkCharsPerLine); }

	public boolean getKeepTimecodes() { return getBoolean(KEEPTIMECODES); }

	public void setKeepTimecodes(boolean keepTimecodes) { setBoolean(KEEPTIMECODES, keepTimecodes); }

	public boolean getSplitWords() { return getBoolean(SPLITWORDS); }

	public void setSplitWords(boolean splitWords) { setBoolean(SPLITWORDS, splitWords); }

	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(KEEPTIMECODES, "Keep timecodes", null);
		desc.add(TIMEFORMAT, "Time format", null);
		desc.add(MAXCHARSPERLINE, "Max characters per line", null);
		desc.add(MAXLINESPERCAPTION, "Max lines per caption", null);
		desc.add(CJKCHARSPERLINE, "Max characters per line for CJK", null);
		desc.add(SPLITWORDS, "Split words so that they don't go over the char limit", null);
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription(ParametersDescription paramDesc) {
		return createEditorDescription("Subtitle Filter Parameters", paramDesc);
	}

	protected EditorDescription createEditorDescription(String caption, ParametersDescription paramDesc) {
		EditorDescription desc = new EditorDescription(caption, true, false);

		CheckboxPart cp = desc.addCheckboxPart(paramDesc.get(KEEPTIMECODES));

		TextInputPart tip = desc.addTextInputPart(paramDesc.get(TIMEFORMAT));
		tip.setAllowEmpty(false);

		SpinInputPart sip = desc.addSpinInputPart(paramDesc.get(MAXLINESPERCAPTION));
		sip.setRange(1, 9999999);

		sip = desc.addSpinInputPart(paramDesc.get(MAXCHARSPERLINE));
		sip.setRange(1, 9999999);

		sip = desc.addSpinInputPart(paramDesc.get(CJKCHARSPERLINE));
		sip.setRange(1, 9999999);

		cp = desc.addCheckboxPart(paramDesc.get(SPLITWORDS));

		return desc;
	}
}

package net.sf.okapi.filters.subtitles;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;
public class SplitCaption implements Iterable<ArrayList<String>> {
    private ArrayList<ArrayList<String>> captions;
    private final boolean dontSplitWords;
    private final int trueMaxChar;

    private SplitCaption(boolean dontSplitWords, int trueMaxChar) {
        this.dontSplitWords = dontSplitWords;
        this.trueMaxChar = trueMaxChar;
        captions = new ArrayList<>();
    }

    private SplitCaption(String s, int maxLine, int maxChar, int widthPerCaption, int numCaptions, boolean dontSplitWords, int trueMaxChar) {
        this(dontSplitWords, trueMaxChar);
        for (String caption : splitEqually(s, numCaptions)) {
            addAndSplitCaption(caption, maxLine, maxChar, false);
        }
    }

    private int numCaptions() {
        return captions.size();
    }

    private void addAndSplitCaptionDontSplitWords(String s, int maxLine, boolean attemptMinFill) {
        int maxChar = minParts(s, maxLine);
        if (attemptMinFill) {
            maxChar = trueMaxChar;
        }
        ArrayList<String> caption = new ArrayList<>();
        String[] captionWords = s.split("\\s+");
        int indexCaptionWords = 0;
        while (true) {
            StringBuilder line = new StringBuilder();
            while (indexCaptionWords < captionWords.length) {
                line.append(captionWords[indexCaptionWords]);
                indexCaptionWords += 1;
                if (line.length() > maxChar || indexCaptionWords == captionWords.length) {
                    break;
                }
                line.append(" ");
            }

            caption.add(line.toString());

            if (indexCaptionWords == captionWords.length) {
                break;
            }
        }

        if (!attemptMinFill && caption.size() < maxLine) {
            addAndSplitCaptionDontSplitWords(s, maxLine, true);
            return;
        }

        captions.add(caption);
    }

    private void addAndSplitCaption(String s, int maxLine, int maxChar, boolean attemptMaxFill) {
        if (dontSplitWords) {
            addAndSplitCaptionDontSplitWords(s, maxLine, false);
            return;
        }
        // assert maxLine
        ArrayList<String> caption = new ArrayList<>();
        String[] captionWords = s.split("\\s+");
        int indexCaptionWords = 0;
        int averageLengthPerLine = s.length() / maxLine;
        while (true) {
            StringBuilder line = new StringBuilder();
            int lastWordIndex = 0;
            while (indexCaptionWords < captionWords.length) {
                line.append(captionWords[indexCaptionWords]);
                indexCaptionWords += 1;
                if ((!attemptMaxFill && line.length() > averageLengthPerLine)
                        || (line.length() > maxChar)
                        || (indexCaptionWords == captionWords.length)) {
                    break;
                }
                lastWordIndex = line.length();
                line.append(" ");
            }
            if (line.length() > maxChar) {
                if (lastWordIndex == 0 || caption.size() == maxLine - 1) {
                    addEqually(s, maxChar, maxLine);
                    return;
                }
                indexCaptionWords -= 1;
                line.delete(lastWordIndex, line.length());
            }

            caption.add(line.toString());

            if (indexCaptionWords == captionWords.length) {
                break;
            }
        }

        if (indexCaptionWords == captionWords.length) {
            captions.add(caption);
            return;
        }

        if (!attemptMaxFill) {
            addAndSplitCaption(s, maxLine, maxChar, true);
            return;
        }

        addEqually(s, maxChar, maxLine);
    }

    private void addEqually(String s, int maxChar, int maxLine) {
        captions.add(splitEquallyIntoMinParts(s, maxChar, maxLine));
    }

    @Override
    public Iterator<ArrayList<String>> iterator() {
        return new Iterator<>() {
            int current = 0;

            @Override
            public void remove() {
                throw new UnsupportedOperationException("The method remove() not supported.");
            }

            @Override
            public ArrayList<String> next() {
                if (current >= captions.size()) {
                    throw new NoSuchElementException("No more content parts.");
                }
                return captions.get(current++);
            }

            @Override
            public boolean hasNext() {
                return (current < captions.size());
            }
        };
    }

    private static ArrayList<String> splitEquallyIntoMinParts(String s, int maxChar, int maxParts) {
        return splitEqually(s, Math.min(maxParts, minParts(s, maxChar)));
    }

    private static ArrayList<String> splitEqually(String s, int parts) {
        int maxChar = s.length() / parts;
        int modLength = modLength(s, parts);
        ArrayList<String> result = new ArrayList<>();
        int index = 0;
        while (index < s.length()) {
            int increment = maxChar;
            if (modLength > 0) {
                increment += 1;
                modLength -= 1;
            }
            result.add(s.substring(index, Math.min(index + increment, s.length())).trim());
            index += increment;
        }
        while (result.size() < parts) {
            result.add("");
        }
        return result;
    }

    private static int modLength(String s, int maxLength) {
        return s.length() % maxLength;
    }

    private static int minParts(String s, int maxLength) {
        return s.length() / maxLength + (modLength(s, maxLength) == 0 ? 0 : 1);
    }

    public static SplitCaption splitCaptionDontSplitWords(String wholeCaption, int numCaptions, int maxLine, int maxChar, int trueMaxChar, boolean attemptMinFill) {
        String[] captionWords = wholeCaption.split("\\s+");

        SplitCaption splitCaption = new SplitCaption(true, trueMaxChar);

        int widthPerCaption = maxLine * maxChar;
        int indexCaptionWords = 0;

        for (int i = 0; i < numCaptions; i++) {
            StringBuilder caption = new StringBuilder();
            int lastWordIndex = 0;

            while (indexCaptionWords < captionWords.length) {
                caption.append(captionWords[indexCaptionWords]);
                indexCaptionWords += 1;
                if (caption.length() >= widthPerCaption || indexCaptionWords == captionWords.length) {
                    break;
                }
                lastWordIndex = caption.length();
                caption.append(" ");
            }
            if (attemptMinFill && caption.length() > widthPerCaption && lastWordIndex != 0) {
                caption.delete(lastWordIndex, caption.length());
                indexCaptionWords -= 1;
            }
            splitCaption.addAndSplitCaptionDontSplitWords(caption.toString(), maxLine, false);
        }

        if (!attemptMinFill && splitCaption.numCaptions() < numCaptions) {
            return splitCaptionDontSplitWords(wholeCaption, numCaptions, maxLine, trueMaxChar, trueMaxChar, true);
        }

        return splitCaption;
    }

    public static SplitCaption splitCaption(String wholeCaption, int numCaptions, int maxLine, int maxChar, boolean attemptMaxFill, boolean attemptMinFill, boolean dontSplitWords) {
        int lenCaption = wholeCaption.length();

        int trueMaxChar = maxChar;
        // If the caption overfills the maximum width, adjust the maximum number of characters per line
        if (lenCaption > numCaptions * maxLine * maxChar) {
            maxChar = minParts(wholeCaption, numCaptions * maxLine);
        }

        int maxWidthPerCaption = maxLine * maxChar;

        String[] captionWords = wholeCaption.split("\\s+");

        // if there will be empty captions when split by word
        if (captionWords.length < numCaptions) {
            return new SplitCaption(wholeCaption, maxLine, maxChar, maxWidthPerCaption, numCaptions, dontSplitWords, trueMaxChar);
        }

        SplitCaption splitCaption = new SplitCaption(dontSplitWords, trueMaxChar);
        int indexCaptionWords = 0;
        int widthPerCaption;
        int captionLengthLeft = lenCaption;

        for (int i = 0; i < numCaptions; i++) {
            if (indexCaptionWords == captionWords.length) {
                // we want to avoid empty captions as much as possible.
                if (attemptMinFill) {
                    // give up
                    return new SplitCaption(wholeCaption, maxLine, maxChar, maxWidthPerCaption, numCaptions, dontSplitWords, trueMaxChar);
                } else {
                    return splitCaption(wholeCaption, numCaptions, maxLine, maxChar, attemptMaxFill, true, dontSplitWords);
                }
            }
            widthPerCaption = captionLengthLeft / (numCaptions - i);
            widthPerCaption = Math.min(widthPerCaption, maxWidthPerCaption);

            StringBuilder caption = new StringBuilder();
            int lastWordIndex = 0;
            while (indexCaptionWords < captionWords.length) {
                caption.append(captionWords[indexCaptionWords]);
                indexCaptionWords += 1;
                if ((!attemptMaxFill && caption.length() >= widthPerCaption)
                        || caption.length() >= maxWidthPerCaption
                        || indexCaptionWords == captionWords.length) {
                    break;
                }
                lastWordIndex = caption.length();
                caption.append(" ");
            }
            if (caption.length() > maxWidthPerCaption) {
                if (lastWordIndex == 0 || splitCaption.numCaptions() == numCaptions - 1) {
                    if (dontSplitWords) {
                        return splitCaptionDontSplitWords(wholeCaption, numCaptions, maxLine, maxChar, trueMaxChar, false);
                    }
                    return new SplitCaption(wholeCaption, maxLine, maxChar, maxWidthPerCaption, numCaptions, dontSplitWords, trueMaxChar);
                }
                caption.delete(lastWordIndex, caption.length());
                indexCaptionWords -= 1;
            }
            if (attemptMinFill && caption.length() > widthPerCaption && lastWordIndex != 0) {
                caption.delete(lastWordIndex, caption.length());
                indexCaptionWords -= 1;
            }
            splitCaption.addAndSplitCaption(caption.toString(), maxLine, maxChar, false);

            captionLengthLeft = captionLengthLeft - caption.length();
        }
        if (indexCaptionWords == captionWords.length) {
            return splitCaption;
        }
        if (attemptMaxFill) {
            return new SplitCaption(wholeCaption, maxLine, maxChar, maxWidthPerCaption, numCaptions, dontSplitWords, trueMaxChar);
        }

        return splitCaption(wholeCaption, numCaptions, maxLine, maxChar, true, attemptMinFill, dontSplitWords);
    }
}

package net.sf.okapi.filters.vtt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;

import net.sf.okapi.common.skeleton.GenericSkeleton;
import net.sf.okapi.common.skeleton.GenericSkeletonPart;
import net.sf.okapi.filters.subtitles.CaptionAnnotation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.ITextUnit;

@RunWith(JUnit4.class)
public class VTTFilterTest {
	private static final String TIME_FORMAT = "HH:mm:ss.SSS";
	private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern(TIME_FORMAT);

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	private final FileLocation location = FileLocation.fromClass(this.getClass());
	
	private VTTFilter filter;
	private FilterTestDriver testDriver;
    private LocaleId locEN = LocaleId.fromString("en");
    private LocaleId locFR = LocaleId.fromString("fr");
	
	@Before
	public void setUp() {
		filter = new VTTFilter();
		assertNotNull(filter);
		
		testDriver = new FilterTestDriver();
		assertNotNull(testDriver);
		
		testDriver.setDisplayLevel(0);
		testDriver.setShowSkeleton(true);

		VTTParameters parameters = filter.getParameters();
		setDefaultParams(parameters);
	}

	public static void setDefaultParams(VTTParameters params) {
		if (params == null) return;

		params.setTimeFormat(TIME_FORMAT);
		params.setMaxLinesPerCaption(2);
		params.setMaxCharsPerLine(42);
	}

	private static void assertCaptionTiming(String begin, String end, CaptionAnnotation.CaptionTiming timing) {
		assertEquals(begin, timing.getBeginTime().format(TIME_FORMATTER));
		assertEquals(end, timing.getEndTime().format(TIME_FORMATTER));
	}

	@Test
	public void testSimple() {
		String snippet = "WEBVTT\n" +
				"\n" +
				"00:00:02.680 --> 00:00:04.720\n" +
				"Thanks everyone\n" +
				"for joining us today.\n" +
				"\n" +
				"00:00:04.800 --> 00:00:06.960\n" +
				"I am so excited\n" +
				"to be with you.";
		List<Event> events = FilterTestDriver.getEvents(filter, snippet, locEN, locFR);

		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertNotNull(tu);
		assertEquals("Thanks everyone for joining us today.", tu.getSource().getFirstContent().getText());
		assertEquals("00:00:02.680 --> 00:00:04.720", ((GenericSkeleton) tu.getSkeleton()).getFirstPart().toString());
		CaptionAnnotation annotation = tu.getAnnotation(CaptionAnnotation.class);
		assertNotNull(annotation);
		assertEquals(2, annotation.getMaxLine());
		assertEquals(42, annotation.getMaxChar());
		assertEquals(1, annotation.getSize());
		assertCaptionTiming("00:00:02.680", "00:00:04.720", annotation.iterator().next());

		tu = FilterTestDriver.getTextUnit(events, 2);
		assertNotNull(tu);
		assertEquals("I am so excited to be with you.", tu.getSource().getFirstContent().getText());
		assertEquals("00:00:04.800 --> 00:00:06.960", ((GenericSkeleton) tu.getSkeleton()).getFirstPart().toString());
		annotation = tu.getAnnotation(CaptionAnnotation.class);
		assertNotNull(annotation);
		assertEquals(2, annotation.getMaxLine());
		assertEquals(42, annotation.getMaxChar());
		assertEquals(1, annotation.getSize());
		assertCaptionTiming("00:00:04.800", "00:00:06.960", annotation.iterator().next());
	}

	@Test
	public void testMergeCaptions() {
		String snippet = "WEBVTT\n" +
				"\n" +
				"00:00:02.680 --> 00:00:04.720\n" +
				"Thanks everyone\n" +
				"for joining us today,\n" +
				"\n" +
				"00:00:04.800 --> 00:00:06.960\n" +
				"I am so excited\n" +
				"to be with you.";
		List<Event> events = FilterTestDriver.getEvents(filter, snippet, locEN, locFR);

		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertNotNull(tu);
		assertEquals("Thanks everyone for joining us today, I am so excited to be with you.", tu.getSource().getFirstContent().getText());
		List<GenericSkeletonPart> parts = ((GenericSkeleton) tu.getSkeleton()).getParts();
		assertEquals(3, parts.size());
		assertEquals("00:00:02.680 --> 00:00:04.720", parts.get(0).toString());
		assertEquals(VTTSkeletonPart.PLACEHOLDER, parts.get(1).toString());
		assertEquals("00:00:04.800 --> 00:00:06.960", parts.get(2).toString());
		CaptionAnnotation annotation = tu.getAnnotation(CaptionAnnotation.class);
		assertNotNull(annotation);
		assertEquals(2, annotation.getMaxLine());
		assertEquals(42, annotation.getMaxChar());
		assertEquals(2, annotation.getSize());
		Iterator<CaptionAnnotation.CaptionTiming> iterator = annotation.iterator();
		assertCaptionTiming("00:00:02.680", "00:00:04.720", iterator.next());
		assertCaptionTiming("00:00:04.800", "00:00:06.960", iterator.next());
	}

	@Test
	public void testMergeCaptionsWithChapters() {
		String snippet = "WEBVTT\n" +
				"\n" +
				"1\n" +
				"00:00:02.680 --> 00:00:04.720\n" +
				"Thanks everyone\n" +
				"for joining us today,\n" +
				"\n" +
				"2\n" +
				"00:00:04.800 --> 00:00:06.960\n" +
				"I am so excited\n" +
				"to be with you.";
		List<Event> events = FilterTestDriver.getEvents(filter, snippet, locEN, locFR);

		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertNotNull(tu);
		assertEquals("Thanks everyone for joining us today, I am so excited to be with you.", tu.getSource().getFirstContent().getText());
		List<GenericSkeletonPart> parts = ((GenericSkeleton) tu.getSkeleton()).getParts();
		assertEquals(4, parts.size());
		assertEquals("00:00:02.680 --> 00:00:04.720", parts.get(0).toString());
		assertEquals(VTTSkeletonPart.PLACEHOLDER, parts.get(1).toString());
		assertEquals("2", parts.get(2).toString());
		assertEquals("00:00:04.800 --> 00:00:06.960", parts.get(3).toString());
		CaptionAnnotation annotation = tu.getAnnotation(CaptionAnnotation.class);
		assertNotNull(annotation);
		assertEquals(2, annotation.getMaxLine());
		assertEquals(42, annotation.getMaxChar());
		assertEquals(2, annotation.getSize());
		Iterator<CaptionAnnotation.CaptionTiming> iterator = annotation.iterator();
		assertCaptionTiming("00:00:02.680", "00:00:04.720", iterator.next());
		assertCaptionTiming("00:00:04.800", "00:00:06.960", iterator.next());
	}

	@Test
	public void testMergeCaptionsWithCueSettings() {
		String snippet = "WEBVTT\n" +
				"\n" +
				"00:00:02.680 --> 00:00:04.720 align:middle line:84%\n" +
				"Thanks everyone\n" +
				"for joining us today,\n" +
				"\n" +
				"00:00:04.800 --> 00:00:06.960 align:middle line:84%\n" +
				"I am so excited\n" +
				"to be with you.";
		List<Event> events = FilterTestDriver.getEvents(filter, snippet, locEN, locFR);

		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertNotNull(tu);
		assertEquals("Thanks everyone for joining us today, I am so excited to be with you.", tu.getSource().getFirstContent().getText());
		List<GenericSkeletonPart> parts = ((GenericSkeleton) tu.getSkeleton()).getParts();
		assertEquals(3, parts.size());
		assertEquals("00:00:02.680 --> 00:00:04.720 align:middle line:84%", parts.get(0).toString());
		assertEquals(VTTSkeletonPart.PLACEHOLDER, parts.get(1).toString());
		assertEquals("00:00:04.800 --> 00:00:06.960 align:middle line:84%", parts.get(2).toString());
		CaptionAnnotation annotation = tu.getAnnotation(CaptionAnnotation.class);
		assertNotNull(annotation);
		assertEquals(2, annotation.getMaxLine());
		assertEquals(42, annotation.getMaxChar());
		assertEquals(2, annotation.getSize());
		Iterator<CaptionAnnotation.CaptionTiming> iterator = annotation.iterator();
		assertCaptionTiming("00:00:02.680", "00:00:04.720", iterator.next());
		assertCaptionTiming("00:00:04.800", "00:00:06.960", iterator.next());
	}

	@Test
	public void testQuotePunctuation() {
		String snippet = "WEBVTT\n" +
				"\n" +
				"00:00:02.680 --> 00:00:04.720\n" +
				"'Thanks everyone\n" +
				"for joining us today.'\n" +
				"\n" +
				"00:00:04.800 --> 00:00:06.960\n" +
				"'I am so excited\n" +
				"to be with you.'";
		List<Event> events = FilterTestDriver.getEvents(filter, snippet, locEN, locFR);

		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertNotNull(tu);
		assertEquals("'Thanks everyone for joining us today.'", tu.getSource().getFirstContent().getText());
		assertEquals("00:00:02.680 --> 00:00:04.720", ((GenericSkeleton) tu.getSkeleton()).getFirstPart().toString());
		CaptionAnnotation annotation = tu.getAnnotation(CaptionAnnotation.class);
		assertNotNull(annotation);
		assertEquals(2, annotation.getMaxLine());
		assertEquals(42, annotation.getMaxChar());
		assertEquals(1, annotation.getSize());
		assertCaptionTiming("00:00:02.680", "00:00:04.720", annotation.iterator().next());

		tu = FilterTestDriver.getTextUnit(events, 2);
		assertNotNull(tu);
		assertEquals("'I am so excited to be with you.'", tu.getSource().getFirstContent().getText());
		assertEquals("00:00:04.800 --> 00:00:06.960", ((GenericSkeleton) tu.getSkeleton()).getFirstPart().toString());
		annotation = tu.getAnnotation(CaptionAnnotation.class);
		assertNotNull(annotation);
		assertEquals(2, annotation.getMaxLine());
		assertEquals(42, annotation.getMaxChar());
		assertEquals(1, annotation.getSize());
		assertCaptionTiming("00:00:04.800", "00:00:06.960", annotation.iterator().next());
	}

	@Test
	public void testVoiceSpans() {
		String snippet = "WEBVTT\n" +
				"\n" +
				"00:00:02.680 --> 00:00:04.720\n" +
				"<v0>Thanks everyone\n" +
				"for joining us today.</v>\n" +
				"\n" +
				"00:00:04.800 --> 00:00:06.960\n" +
				"<v1>I am so excited\n" +
				"to be with you.</v>";
		List<Event> events = FilterTestDriver.getEvents(filter, snippet, locEN, locFR);

		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertNotNull(tu);
		assertEquals("<v0>Thanks everyone for joining us today.</v>", tu.getSource().getFirstContent().getText());
		assertEquals("00:00:02.680 --> 00:00:04.720", ((GenericSkeleton) tu.getSkeleton()).getFirstPart().toString());
		CaptionAnnotation annotation = tu.getAnnotation(CaptionAnnotation.class);
		assertNotNull(annotation);
		assertEquals(2, annotation.getMaxLine());
		assertEquals(42, annotation.getMaxChar());
		assertEquals(1, annotation.getSize());
		assertCaptionTiming("00:00:02.680", "00:00:04.720", annotation.iterator().next());

		tu = FilterTestDriver.getTextUnit(events, 2);
		assertNotNull(tu);
		assertEquals("<v1>I am so excited to be with you.</v>", tu.getSource().getFirstContent().getText());
		assertEquals("00:00:04.800 --> 00:00:06.960", ((GenericSkeleton) tu.getSkeleton()).getFirstPart().toString());
		annotation = tu.getAnnotation(CaptionAnnotation.class);
		assertNotNull(annotation);
		assertEquals(2, annotation.getMaxLine());
		assertEquals(42, annotation.getMaxChar());
		assertEquals(1, annotation.getSize());
		assertCaptionTiming("00:00:04.800", "00:00:06.960", annotation.iterator().next());
	}

	@Test
	public void testEmptyCaption() {
		String snippet = "WEBVTT\n" +
				"\n" +
				"00:00:02.680 --> 00:00:04.720\n" +
				"\n" +
				"\n" +
				"00:00:04.800 --> 00:00:06.960\n" +
				"I am so excited\n" +
				"to be with you.";
		List<Event> events = FilterTestDriver.getEvents(filter, snippet, locEN, locFR);

		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertNotNull(tu);
		assertEquals("", tu.getSource().getFirstContent().getText());
		assertEquals("00:00:02.680 --> 00:00:04.720", ((GenericSkeleton) tu.getSkeleton()).getFirstPart().toString());
		CaptionAnnotation annotation = tu.getAnnotation(CaptionAnnotation.class);
		assertNotNull(annotation);
		assertEquals(2, annotation.getMaxLine());
		assertEquals(42, annotation.getMaxChar());
		assertEquals(1, annotation.getSize());
		assertCaptionTiming("00:00:02.680", "00:00:04.720", annotation.iterator().next());

		tu = FilterTestDriver.getTextUnit(events, 2);
		assertNotNull(tu);
		assertEquals("I am so excited to be with you.", tu.getSource().getFirstContent().getText());
		assertEquals("00:00:04.800 --> 00:00:06.960", ((GenericSkeleton) tu.getSkeleton()).getFirstPart().toString());
		annotation = tu.getAnnotation(CaptionAnnotation.class);
		assertNotNull(annotation);
		assertEquals(2, annotation.getMaxLine());
		assertEquals(42, annotation.getMaxChar());
		assertEquals(1, annotation.getSize());
		assertCaptionTiming("00:00:04.800", "00:00:06.960", annotation.iterator().next());
	}
}
	
/*
 * =============================================================================
 *   Copyright (C) 2010-2017 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */

package net.sf.okapi.filters.idml;

import net.sf.okapi.common.filters.fontmappings.FontMappings;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;

class StyleRange {

    static final QName APPLIED_PARAGRAPH_STYLE = Namespaces.getDefaultNamespace().getQName("AppliedParagraphStyle");
    static final String PARAGRAPH_STYLE_DEFAULT_VALUE = "ParagraphStyle/$ID/NormalParagraphStyle";

    static final QName APPLIED_CHARACTER_STYLE = Namespaces.getDefaultNamespace().getQName("AppliedCharacterStyle");
    static final String CHARACTER_STYLE_DEFAULT_VALUE = "CharacterStyle/$ID/[No character style]";
    static final String STYLE_NONE_VALUE = "n";

    static final QName PARAGRAPH_STYLE_RANGE = Namespaces.getDefaultNamespace().getQName("ParagraphStyleRange");
    static final QName CHARACTER_STYLE_RANGE = Namespaces.getDefaultNamespace().getQName("CharacterStyleRange");

    private final XMLEventFactory eventFactory;
    private final QName name;
    private final List<Attribute> attributes;
    private final Properties properties;

    StyleRange(final XMLEventFactory eventFactory, final QName name, final List<Attribute> attributes, final Properties properties) {
        this.eventFactory = eventFactory;
        this.name = name;
        this.attributes = attributes;
        this.properties = properties;
    }

    static StyleRange defaultParagraphStyleRange(XMLEventFactory eventFactory) {
        List<Attribute> attributes = singletonList(eventFactory.createAttribute(APPLIED_PARAGRAPH_STYLE, PARAGRAPH_STYLE_DEFAULT_VALUE));
        Properties properties = new Properties.Empty(eventFactory);

        return new StyleRange(eventFactory, PARAGRAPH_STYLE_RANGE, attributes, properties);
    }

    static StyleRange defaultCharacterStyleRange(XMLEventFactory eventFactory) {
        List<Attribute> attributes = singletonList(eventFactory.createAttribute(APPLIED_CHARACTER_STYLE, CHARACTER_STYLE_DEFAULT_VALUE));
        Properties properties = new Properties.Empty(eventFactory);

        return new StyleRange(eventFactory, CHARACTER_STYLE_RANGE, attributes, properties);
    }

    XMLEventFactory eventFactory() {
        return this.eventFactory;
    }

    QName name() {
        return this.name;
    }

    List<Attribute> attributes() {
        return attributes;
    }

    Properties properties() {
        return properties;
    }

    boolean isSubsetOf(StyleRange other) {
        if (attributes().isEmpty() && !other.attributes().isEmpty()
                || properties().properties().isEmpty() && !other.properties().properties().isEmpty()) {
            return false;
        }

        outerAttributes:	for (Attribute attribute : attributes()) {
            for (Attribute otherAttribute : other.attributes()) {
                if (otherAttribute.equals(attribute)) {
                    continue outerAttributes;
                }
            }
            return false;
        }

        outerProperties:	for (Property property : properties().properties()) {
            for (Property otherProperty : other.properties().properties()) {
                if (otherProperty.equals(property)) {
                    continue outerProperties;
                }
            }
            return false;
        }

        return true;
    }

    void apply(final FontMappings fontMappings) {
        this.properties.apply(fontMappings);
    }

    StyleRange mergedWith(final StyleRange styleRange) {
        final StyleRange sr;
        if (equals(styleRange)) {
            sr = styleRange;
        } else {
            sr = new StyleRange(
                styleRange.eventFactory(),
                styleRange.name(),
                mergedAttributesWith(styleRange.attributes()),
                this.properties.mergedWith(styleRange.properties())
            );
        }
        return sr;
    }

    private List<Attribute> mergedAttributesWith(final List<Attribute> attributes) {
        final List<Attribute> merged;
        if (areAttributesEqual(this.attributes, attributes)) {
            return attributes;
        } else {
            final Set<Attribute> intersection = this.attributes.stream()
                .filter(a -> attributes.contains(a))
                .collect(Collectors.toSet());
            final Set<Attribute> theseAttributes = this.attributes.stream()
                .filter(a -> !intersection.contains(a))
                .collect(Collectors.toSet());
            final Set<Attribute> otherAttributes = attributes.stream()
                .filter(a -> !intersection.contains(a))
                .collect(Collectors.toSet());
            merged = new ArrayList<>(intersection);
            final Iterator<Attribute> theseAttributesIterator = theseAttributes.iterator();
            while (theseAttributesIterator.hasNext()) {
                final Attribute ta = theseAttributesIterator.next();
                final Iterator<Attribute> otherAttributesIterator = otherAttributes.iterator();
                while (otherAttributesIterator.hasNext()) {
                    final Attribute oa = otherAttributesIterator.next();
                    if (ta.getName().equals(oa.getName())) {
                        merged.add(oa);
                        theseAttributesIterator.remove();
                        otherAttributesIterator.remove();
                        break;
                    }
                }
            }
            merged.addAll(theseAttributes);
            merged.addAll(otherAttributes);
            merged.sort(new AttributesComparator());
        }
        return merged;
    }

    List<XMLEvent> asStyleRangeStart() {
        final List<XMLEvent> events = new ArrayList<>();
        events.add(this.eventFactory.createStartElement(this.name, this.attributes.iterator(), null));
        events.addAll(this.properties.getEvents());
        return events;
    }

    List<XMLEvent> asStyleRangeEnd() {
        return Collections.singletonList(this.eventFactory.createEndElement(this.name, null));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (null == o || getClass() != o.getClass()) return false;

        StyleRange that = (StyleRange) o;

        return areAttributesEqual(attributes(), that.attributes())
                && Objects.equals(properties(), that.properties());
    }

    /**
     * Checks whether provided attributes are equal.
     *
     * Replaces the conservative org.codehaus.stax2.ri.evt.AttributeEventImpl#equals(java.lang.Object) implementation,
     * which also checks for the internal mWasSpecified flag.
     *
     * @param thisAttributes This attributes
     * @param thatAttributes That attributes
     *
     * @return {@code true}  If attributes are equal
     *         {@code false} Otherwise
     */
    private boolean areAttributesEqual(List<Attribute> thisAttributes, List<Attribute> thatAttributes) {
        if (thisAttributes == thatAttributes) return true;
        if (null == thisAttributes || null == thatAttributes) return false;

        if (thisAttributes.size() != thatAttributes.size()) return false;

        Iterator<Attribute> thisAttributesIterator = thisAttributes.iterator();
        Iterator<Attribute> thatAttributesIterator = thatAttributes.iterator();

        while (thisAttributesIterator.hasNext() && thatAttributesIterator.hasNext()) {
            Attribute thisAttribute = thisAttributesIterator.next();
            Attribute thatAttribute = thatAttributesIterator.next();

            if (!thisAttribute.getName().equals(thatAttribute.getName())
                    || !thisAttribute.getValue().equals(thatAttribute.getValue())) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(attributes(), properties());
    }

    static class AttributesComparator implements Comparator<Attribute> {
        @Override
        public int compare(final Attribute a1, final Attribute a2) {
            return a1.getName().getLocalPart().compareTo(a2.getName().getLocalPart());
        }
    }

    static class StyleRangeBuilder implements Builder<StyleRange> {
        private final XMLEventFactory eventFactory;
        private final AttributesComparator attributesComparator;
        private QName name;
        private List<Attribute> attributes;
        private Properties properties;

        StyleRangeBuilder(final XMLEventFactory eventFactory, final AttributesComparator attributesComparator) {
            this.eventFactory = eventFactory;
            this.attributesComparator = attributesComparator;
        }

        StyleRangeBuilder setName(final QName name) {
            this.name = name;
            return this;
        }

        StyleRangeBuilder setAttributes(List<Attribute> attributes) {
            this.attributes = attributes;
            return this;
        }

        StyleRangeBuilder setProperties(Properties properties) {
            this.properties = properties;
            return this;
        }

        @Override
        public StyleRange build() {
            this.attributes.sort(this.attributesComparator);
            return new StyleRange(this.eventFactory, this.name, this.attributes, this.properties);
        }
    }
}

/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.idml;

import net.sf.okapi.common.ISkeleton;
import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextUnit;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.events.XMLEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

final class TextElementMapping {
    private static final String SPECIAL_CHARACTER = "special-character";
    private static final String DASH = "-";
    private static final String TAG_OPENING = "<";
    private static final String TAG_CLOSING = "/>";

    private final Parameters parameters;
    private final XMLEventFactory eventFactory;
    private final IdGenerator textUnitIds;
    private TextFragment textFragment;
    private Map<Integer, MarkupRange> codeMap;
    private int nextCodeId;

    TextElementMapping(
        final Parameters parameters,
        final XMLEventFactory eventFactory,
        final IdGenerator textUnitIds
    ) {
        this.parameters = parameters;
        this.eventFactory = eventFactory;
        this.textUnitIds = textUnitIds;
    }

    ITextUnit textUnitFor(final Element textElement) {
        this.codeMap = new HashMap<>();
        this.nextCodeId = 1;
        this.textFragment = new TextFragment();
        mapCodesIn(textElement.innerEvents());
        final ITextUnit textUnit = new TextUnit(this.textUnitIds.createId());
        textUnit.setPreserveWhitespaces(true);
        textUnit.setSource(new TextContainer(this.textFragment));
        final ISkeleton skeleton = new TextSkeleton(textElement, this.codeMap);
        skeleton.setParent(textUnit);
        textUnit.setSkeleton(skeleton);
        return textUnit;
    }

    private void mapCodesIn(final List<XMLEvent> events) {
        for (final XMLEvent event : events) {
            if (event.isProcessingInstruction()) {
                addIsolatedCodeFor(new SpecialCharacter.Instruction(event));
                continue;
            }
            if (!event.isCharacters()) {
                // only processing instructions and characters are allowed
                throw new IllegalStateException(ParsingIdioms.UNEXPECTED_STRUCTURE);
            }
            isolateSpecialCharactersIn(event.asCharacters().getData());
        }
    }

    private void addIsolatedCodeFor(final MarkupRange markupRange) {
        final String codeType = CodeTypes.createCodeType(markupRange);
        final String codeData = TAG_OPENING + SPECIAL_CHARACTER + DASH + this.nextCodeId + TAG_CLOSING;
        final Code code = new Code(TextFragment.TagType.PLACEHOLDER, codeType, codeData);
        code.setId(this.nextCodeId);
        this.textFragment.append(code);
        this.codeMap.put(this.nextCodeId, markupRange);
        this.nextCodeId++;
    }

    private void isolateSpecialCharactersIn(final String characters) {
        for (int i = 0; i < characters.length(); i++) {
            final String character = String.valueOf(characters.charAt(i));
            final Matcher m = this.parameters.specialCharacterPattern().matcher(character);
            if (m.matches()) {
                SpecialCharacter.Type specialCharacterType = SpecialCharacter.Type.fromDefaultString(character);
                if (this.parameters.getSkipDiscretionaryHyphens() && SpecialCharacter.Type.DISCRETIONARY_HYPHEN == specialCharacterType) {
                    continue;
                }
                final SpecialCharacter specialCharacter = new SpecialCharacter.Default(
                    this.eventFactory.createCharacters(character)
                );
                addIsolatedCodeFor(specialCharacter);
            } else {
                this.textFragment.append(character);
            }
        }
    }
}

/*
 * =============================================================================
 *   Copyright (C) 2010-2017 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */

package net.sf.okapi.filters.idml;

import javax.xml.namespace.QName;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class StoryChildElementsWriter {
    private final List<QName> effectiveStyleRanges;
    private final List<XMLEvent> events;
    private StyleRanges currentStyleRanges;

    StoryChildElementsWriter(final QName... effectiveStyleRanges) {
        this(
            Arrays.asList(effectiveStyleRanges),
            new ArrayList<>()
        );
    }

    StoryChildElementsWriter(final List<QName> effectiveStyleRanges, final List<XMLEvent> events) {
        this.effectiveStyleRanges = effectiveStyleRanges;
        this.events = events;
    }

    List<QName> effectiveStyleRanges() {
        return this.effectiveStyleRanges;
    }

    List<XMLEvent> write(List<StoryChildElement> storyChildElements) {
        this.events.clear();
        for (StoryChildElement storyChildElement : storyChildElements) {
            if (!(storyChildElement instanceof StoryChildElement.StyledTextElement)) {
                writeAsNonStyledTextElement(storyChildElement);
                continue;
            }
            writeAsStyledTextElement(storyChildElement);
        }
        return this.events;
    }

    private void writeAsNonStyledTextElement(final StoryChildElement storyChildElement) {
        if (null != currentStyleRanges) {
            writeClosingStyleRanges();
        }
        this.events.addAll(storyChildElement.getEvents());
        if (null != currentStyleRanges) {
            writeOpeningStyleRanges();
        }
    }

    private void writeAsStyledTextElement(final StoryChildElement storyChildElement) {
        final StyleRanges styleRanges = ((StoryChildElement.StyledTextElement) storyChildElement).styleRanges();
        if (null == this.currentStyleRanges) {
            this.currentStyleRanges = styleRanges;
            writeOpeningStyleRanges();
        } else if (!currentStyleRanges.paragraphStyleRange().equals(styleRanges.paragraphStyleRange())) {
            writeClosingStyleRanges();
            this.currentStyleRanges = styleRanges;
            writeOpeningStyleRanges();
        } else if (!currentStyleRanges.characterStyleRange().equals(styleRanges.characterStyleRange())) {
            if (this.effectiveStyleRanges.contains(StyleRange.CHARACTER_STYLE_RANGE)) {
                this.events.addAll(this.currentStyleRanges.characterStyleRange().asStyleRangeEnd());
            }
            this.currentStyleRanges = styleRanges;
            if (this.effectiveStyleRanges.contains(StyleRange.CHARACTER_STYLE_RANGE)) {
                this.events.addAll(this.currentStyleRanges.characterStyleRange().asStyleRangeStart());
            }
        }
        this.events.addAll(storyChildElement.getEvents());
    }

    private void writeOpeningStyleRanges() {
        if (this.effectiveStyleRanges.contains(StyleRange.PARAGRAPH_STYLE_RANGE)) {
            this.events.addAll(this.currentStyleRanges.paragraphStyleRange().asStyleRangeStart());
        }
        if (this.effectiveStyleRanges.contains(StyleRange.CHARACTER_STYLE_RANGE)) {
            this.events.addAll(this.currentStyleRanges.characterStyleRange().asStyleRangeStart());
        }
    }

    private void writeClosingStyleRanges() {
        if (this.effectiveStyleRanges.contains(StyleRange.CHARACTER_STYLE_RANGE)) {
            this.events.addAll(this.currentStyleRanges.characterStyleRange().asStyleRangeEnd());
        }
        if (this.effectiveStyleRanges.contains(StyleRange.PARAGRAPH_STYLE_RANGE)) {
            this.events.addAll(this.currentStyleRanges.paragraphStyleRange().asStyleRangeEnd());
        }
    }
}

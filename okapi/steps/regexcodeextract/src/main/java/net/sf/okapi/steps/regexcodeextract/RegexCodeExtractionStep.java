/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.steps.regexcodeextract;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.encoder.EncoderContext;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.encoder.IEncoder;
import net.sf.okapi.common.filters.InlineCodeFinder;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextUnitUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;

/**
 * Step that converts a part of the text that matches with one of the given regular expressions
 * to a stand-alone code. This step makes use of {@link InlineCodeFinder}.
 */
@UsingParameters(RegexCodeExtractionStepParameters.class)
public class RegexCodeExtractionStep extends BasePipelineStep {
  private final Logger logger = LoggerFactory.getLogger(getClass());

  protected RegexCodeExtractionStepParameters params  = new RegexCodeExtractionStepParameters();

  protected EncoderManager encoderManager;
  protected IEncoder encoder;
  protected String mimeType;
  protected String lineBreak = System.lineSeparator();

  public RegexCodeExtractionStep() {
    encoderManager = new EncoderManager();
    encoderManager.setAllKnownMappings();
  }

  @Override
  public String getName() {
    return "Regex-based Inline Code Generator, using InlineCodeFinder.";
  }

  @Override
  public String getDescription() {
    return "Converts spans of the text units' source content that match any of the given regular expressions into inline codes. "
            + "Expects: filter events. Sends back: filter events.";
  }

  @Override
  public RegexCodeExtractionStepParameters getParameters() {
    return params;
  }

  @Override
  public void setParameters(IParameters params) {
    this.params = (RegexCodeExtractionStepParameters) params;
  }

  @Override
  protected Event handleStartDocument(Event event) {
    StartDocument sd = event.getStartDocument();

    // Drop the current encoder if mimeType of encoding have changed
    if ((mimeType != null) && !mimeType.equals(sd.getMimeType())) {
      encoder = null;
    }

    mimeType = sd.getMimeType();
    lineBreak = sd.getLineBreak();

    return super.handleStartDocument(event);
  }


  @Override
  protected Event handleTextUnit(Event event) {
    ITextUnit tu = event.getTextUnit();

    if (!tu.isTranslatable() ) {
      return event;
    }

    TextContainer tc = tu.getSource();
    if (tc.getSegments() == null ) {
      logger.warn("getSource().getSegments() on TU (id={}) returned null. Skipping the TU.", tu.getId());
      return event;
    }

    for (Segment seg : tc.getSegments() ) {
      TextFragment tf = seg.text;
      params.codeFinder.process(tf);
      logger.debug("After inline code generation: {}", TextUnitUtil.toText(tf));

      // Escape inline code content
      for (Code code : tf.getCodes()) {
        // Escape the data of the new inline code (and only them)
        if (code.getType().equals(InlineCodeFinder.TAGTYPE)) {
          if (encoder == null) {
            encoderManager.setDefaultOptions(getParameters(), StandardCharsets.UTF_16.name(), lineBreak);
            encoderManager.updateEncoder(mimeType);
            encoder = encoderManager.getEncoder();
          }
          code.setData(encoder.encode(code.getData(), EncoderContext.INLINE));
        }
      }
    }
    return event;
  }
}

echo "===== Running example01 ====="
java -cp .;../lib/*;example01/target/* Main myFile.html -s pseudo myFile.out-pseudo.html
java -cp .;../lib/*;example01/target/* Main myFile.html -s upper myFile.out-upper.html
java -cp .;../lib/*;example01/target/* Main myFile.html -s pseudo -s upper myFile.out-both.html
java -cp .;../lib/*;example01/target/* Main myFile.odt -s pseudo
java -cp .;../lib/*;example01/target/* Main myFile.properties -s pseudo
java -cp .;../lib/*;example01/target/* Main myFile.xml -s pseudo

echo "===== Running example02 ====="
java -cp .;../lib/*;example02/target/* Main myFile.odt

echo "===== Running example03 ====="
java -cp .;../lib/*;example03/target/* Main

echo "===== Running example04 ====="
java -cp .;../lib/*;example04/target/* Main

echo "===== Running example05 ====="
java -cp .;../lib/*;example05/target/* Main

rem echo "===== Running example06 ====="
rem java -cp .;../lib/*;example06/target/* Main

rem echo "===== Running example07 ====="
rem java -cp .;../lib/*;example07/target/* Main

pause

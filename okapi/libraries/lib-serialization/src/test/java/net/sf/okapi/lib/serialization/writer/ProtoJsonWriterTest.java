/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.serialization.writer;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.DummyFilter;
import net.sf.okapi.common.filters.FilterTestDriver;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class ProtoJsonWriterTest {
	private static final String SIMPLE_OUTPUT = "{\"startDocument\":{\"nameable\":{\"resource\":{\"id\":\"sd1\",\"properties\":[]},\"name\":\"\",\"type\":\"\",\"mimeType\":\"text/xml\",\"isTranslatable\":true,\"preserveWhitespaces\":false},\"lineBreak\":\"\",\"locale\":\"en\",\"encoding\":\"\",\"multilingual\":true,\"hasUtf8Bom\":false,\"parameters\":\"\"}}\n" +
			"{\"textUnit\":{\"nameable\":{\"resource\":{\"id\":\"tu1\",\"properties\":[]},\"name\":\"\",\"type\":\"\",\"mimeType\":\"\",\"isTranslatable\":true,\"preserveWhitespaces\":false},\"referenceable\":{\"refCount\":0},\"parentId\":\"\",\"source\":{\"nameable\":{\"resource\":{\"id\":\"\",\"properties\":[]},\"name\":\"\",\"type\":\"\",\"mimeType\":\"\",\"isTranslatable\":true,\"preserveWhitespaces\":false},\"locale\":\"en\",\"parts\":[{\"resource\":{\"id\":\"0\",\"properties\":[]},\"text\":{\"text\":\"Source text\",\"codes\":[],\"balanced\":false},\"whitespaceStrategy\":\"INHERIT\",\"segment\":true}],\"segApplied\":false,\"altTrans\":[],\"notes\":[]},\"targets\":{\"fr\":{\"nameable\":{\"resource\":{\"id\":\"\",\"properties\":[]},\"name\":\"\",\"type\":\"\",\"mimeType\":\"\",\"isTranslatable\":true,\"preserveWhitespaces\":false},\"locale\":\"fr\",\"parts\":[{\"resource\":{\"id\":\"0\",\"properties\":[]},\"text\":{\"text\":\"Target text\",\"codes\":[],\"balanced\":false},\"whitespaceStrategy\":\"INHERIT\",\"segment\":true}],\"segApplied\":false,\"altTrans\":[],\"notes\":[]}},\"notes\":[]}}\n" +
			"{\"textUnit\":{\"nameable\":{\"resource\":{\"id\":\"tu2\",\"properties\":[]},\"name\":\"\",\"type\":\"\",\"mimeType\":\"\",\"isTranslatable\":true,\"preserveWhitespaces\":false},\"referenceable\":{\"refCount\":0},\"parentId\":\"\",\"source\":{\"nameable\":{\"resource\":{\"id\":\"\",\"properties\":[]},\"name\":\"\",\"type\":\"\",\"mimeType\":\"\",\"isTranslatable\":true,\"preserveWhitespaces\":false},\"locale\":\"en\",\"parts\":[{\"resource\":{\"id\":\"0\",\"properties\":[]},\"text\":{\"text\":\"Source text 2\",\"codes\":[],\"balanced\":false},\"whitespaceStrategy\":\"INHERIT\",\"segment\":true}],\"segApplied\":false,\"altTrans\":[],\"notes\":[]},\"targets\":{},\"notes\":[]}}\n" +
			"{\"endDocument\":{\"resource\":{\"id\":\"ed1\",\"properties\":[]}}}\n";
	private static final String SEGMENTED_OUTPUT = "{\"startDocument\":{\"nameable\":{\"resource\":{\"id\":\"sd1\",\"properties\":[]},\"name\":\"\",\"type\":\"\",\"mimeType\":\"text/xml\",\"isTranslatable\":true,\"preserveWhitespaces\":false},\"lineBreak\":\"\",\"locale\":\"en\",\"encoding\":\"\",\"multilingual\":true,\"hasUtf8Bom\":false,\"parameters\":\"\"}}\n" +
			"{\"textUnit\":{\"nameable\":{\"resource\":{\"id\":\"tu1\",\"properties\":[]},\"name\":\"\",\"type\":\"\",\"mimeType\":\"\",\"isTranslatable\":true,\"preserveWhitespaces\":false},\"referenceable\":{\"refCount\":0},\"parentId\":\"\",\"source\":{\"nameable\":{\"resource\":{\"id\":\"\",\"properties\":[]},\"name\":\"\",\"type\":\"\",\"mimeType\":\"\",\"isTranslatable\":true,\"preserveWhitespaces\":false},\"locale\":\"en\",\"parts\":[{\"resource\":{\"id\":\"0\",\"properties\":[]},\"text\":{\"text\":\"First segment for SRC.\",\"codes\":[],\"balanced\":false},\"whitespaceStrategy\":\"INHERIT\",\"segment\":true},{\"resource\":{\"id\":\"1\",\"properties\":[]},\"text\":{\"text\":\" \",\"codes\":[],\"balanced\":false},\"whitespaceStrategy\":\"INHERIT\",\"segment\":false},{\"resource\":{\"id\":\"2\",\"properties\":[]},\"text\":{\"text\":\"Second segment for SRC\",\"codes\":[],\"balanced\":false},\"whitespaceStrategy\":\"INHERIT\",\"segment\":true}],\"segApplied\":true,\"altTrans\":[],\"notes\":[]},\"targets\":{\"fr\":{\"nameable\":{\"resource\":{\"id\":\"\",\"properties\":[]},\"name\":\"\",\"type\":\"\",\"mimeType\":\"\",\"isTranslatable\":true,\"preserveWhitespaces\":false},\"locale\":\"fr\",\"parts\":[{\"resource\":{\"id\":\"0\",\"properties\":[]},\"text\":{\"text\":\"First segment for TRG.\",\"codes\":[],\"balanced\":false},\"whitespaceStrategy\":\"INHERIT\",\"segment\":true},{\"resource\":{\"id\":\"1\",\"properties\":[]},\"text\":{\"text\":\" \",\"codes\":[],\"balanced\":false},\"whitespaceStrategy\":\"INHERIT\",\"segment\":false},{\"resource\":{\"id\":\"2\",\"properties\":[]},\"text\":{\"text\":\"Second segment for TRG\",\"codes\":[],\"balanced\":false},\"whitespaceStrategy\":\"INHERIT\",\"segment\":true}],\"segApplied\":true,\"altTrans\":[],\"notes\":[]}},\"notes\":[]}}\n" +
			"{\"endDocument\":{\"resource\":{\"id\":\"ed1\",\"properties\":[]}}}\n";
	private static final String CODES_OUTPUT = "{\"startDocument\":{\"nameable\":{\"resource\":{\"id\":\"sd1\",\"properties\":[]},\"name\":\"\",\"type\":\"\",\"mimeType\":\"text\",\"isTranslatable\":true,\"preserveWhitespaces\":false},\"lineBreak\":\"\",\"locale\":\"en\",\"encoding\":\"\",\"multilingual\":true,\"hasUtf8Bom\":false,\"parameters\":\"\"}}\n" +
			"{\"textUnit\":{\"nameable\":{\"resource\":{\"id\":\"id1\",\"properties\":[]},\"name\":\"\",\"type\":\"\",\"mimeType\":\"\",\"isTranslatable\":true,\"preserveWhitespaces\":false},\"referenceable\":{\"refCount\":0},\"parentId\":\"\",\"source\":{\"nameable\":{\"resource\":{\"id\":\"\",\"properties\":[]},\"name\":\"\",\"type\":\"\",\"mimeType\":\"\",\"isTranslatable\":true,\"preserveWhitespaces\":false},\"locale\":\"en\",\"parts\":[{\"resource\":{\"id\":\"0\",\"properties\":[]},\"text\":{\"text\":\"Source code \",\"codes\":[{\"id\":1,\"tagType\":\"PLACEHOLDER\",\"codeType\":\"z\",\"data\":\"@#$0\",\"flag\":0,\"position\":12,\"isolated\":false,\"added\":false,\"properties\":[],\"inlineAnnotations\":{}}],\"balanced\":false},\"whitespaceStrategy\":\"INHERIT\",\"segment\":true}],\"segApplied\":false,\"altTrans\":[],\"notes\":[]},\"targets\":{\"fr\":{\"nameable\":{\"resource\":{\"id\":\"\",\"properties\":[]},\"name\":\"\",\"type\":\"\",\"mimeType\":\"\",\"isTranslatable\":true,\"preserveWhitespaces\":false},\"locale\":\"fr\",\"parts\":[{\"resource\":{\"id\":\"0\",\"properties\":[]},\"text\":{\"text\":\"Target code \",\"codes\":[{\"id\":1,\"tagType\":\"PLACEHOLDER\",\"codeType\":\"z\",\"data\":\"@#$0\",\"flag\":0,\"position\":12,\"isolated\":false,\"added\":false,\"properties\":[],\"inlineAnnotations\":{}}],\"balanced\":false},\"whitespaceStrategy\":\"INHERIT\",\"segment\":true}],\"segApplied\":false,\"altTrans\":[],\"notes\":[]}},\"notes\":[]}}\n" +
			"{\"endDocument\":{\"resource\":{\"id\":\"ed1\",\"properties\":[]}}}\n";
	private DummyFilter filter;
	private LocaleId locEN = LocaleId.fromString("en");
	private LocaleId locFR = LocaleId.fromString("fr");

	@Before
	public void setUp() {
		filter = new DummyFilter();
	}

	@Test
	public void testSimpleOutput() {
		String result = rewrite(FilterTestDriver.getEvents(filter, "##def##", locEN, locFR), locFR);
		assertEquals(SIMPLE_OUTPUT, result);
	}

	@Test
	public void testSegmentedOutput() {
		String result = rewrite(FilterTestDriver.getEvents(filter, "##seg##", locEN, locFR), locFR);
		assertEquals(SEGMENTED_OUTPUT, result);
	}
	
	@Test
	public void testCodesOutput() {
		String result = rewrite(FilterTestDriver.getEvents(filter, "Source code @#$0\nTarget code @#$0", locEN, locFR), locFR);
		assertEquals(CODES_OUTPUT, result);
	}

	private String rewrite(ArrayList<Event> list, LocaleId trgLang) {
		ProtoBufferWriter writer = new ProtoBufferWriter();
		writer.setOptions(trgLang, null);
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		writer.setOutput(output);
		for (Event event : list) {
			writer.handleEvent(event);
		}
		writer.close();
		return output.toString();
	}
}

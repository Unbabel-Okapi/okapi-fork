/*
 * ====================================================================
 *   Copyright (C) $time.year by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =====================================================================
 */

package net.sf.okapi.lib.serialization;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IResource;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.annotation.*;
import net.sf.okapi.common.query.MatchType;
import net.sf.okapi.common.resource.*;
import net.sf.okapi.common.resource.TextFragment.TagType;
import net.sf.okapi.proto.EndSubDocument;
import net.sf.okapi.proto.Events;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map.Entry;

public class Proto2Okapi {
	static TagType toTagType(net.sf.okapi.proto.TextFragment.TagType tt) {
		switch (tt) {
			case CLOSING: return TagType.CLOSING;
			case OPENING: return TagType.OPENING;
			case PLACEHOLDER: return TagType.PLACEHOLDER;
			default: return TagType.valueOf("ERROR");
		}
	}

	static Code toCode(net.sf.okapi.proto.Code code) {
		Code result = new Code(toTagType(code.getTagType()), code.getCodeType(), code.getData());
		result.setId(code.getId());
		result.setFlag(code.getFlag());
		result.setDisplayText(code.getDisplayText());
		result.setOriginalId(code.getOriginalId());
		result.setAdded(code.getAdded());

		if (code.hasOuterData()) {
			result.setOuterData(code.getOuterData());
		}

		if (code.hasMergedData()) {
			result.setMergedData(code.getMergedData());
		}

		if (code.getInlineAnnotationsCount() > 0) {
			for (Entry<String, String> ann : code.getInlineAnnotationsMap().entrySet()) {
				// normal properties handled below
				if (ann.getKey().equals(Code.PROPERTIES)) {
					continue;
				}
				GenericAnnotations.addAnnotations(result, new GenericAnnotations(ann.getValue()));
			}
		}

		for (net.sf.okapi.proto.Property property : code.getPropertiesList()) {
			result.setProperty(toProperty(property));
		}

		return result;
	}

	static TextFragment toTextFragment(net.sf.okapi.proto.TextFragment tf) {
		TextFragment result = new TextFragment(tf.getText());
		int codeCount = 0;
		for (net.sf.okapi.proto.Code code : tf.getCodesList()) {			
			result.insert(code.getPosition() + (codeCount*2), toCode(code));
			codeCount++;
		}
		result.balanceMarkers();
		return result;
	}

	static TextPart toTextPart(net.sf.okapi.proto.TextPart part) {
		TextPart result;
		if (part.getSegment()) {
			result = new Segment(part.getResource().getId(), toTextFragment(part.getText()));
		} else {
			result = new TextPart(toTextFragment(part.getText()));
		}
		setIResource(result, part.getResource());
		result.setOriginalId(part.getOriginalId());
		result.setWhitespaceStrategy(toWhitespaceStrategy(part.getWhitespaceStrategy()));
		return result;
	}

	static WhitespaceStrategy toWhitespaceStrategy(net.sf.okapi.proto.TextPart.WhitespaceStrategy whitespaceStrategy) {
		switch (whitespaceStrategy.getNumber()) {
			case 0:
				return WhitespaceStrategy.INHERIT;
			case 1:
				return WhitespaceStrategy.PRESERVE;
			case 2:
				return WhitespaceStrategy.NORMALIZE;
			default:
				return WhitespaceStrategy.INHERIT;
		}
	}

	static Property toProperty(net.sf.okapi.proto.Property prop) {
		EnumSet<Property.Type> types = EnumSet.noneOf(Property.Type.class);

		for (net.sf.okapi.proto.Property.PropertyType type : prop.getTypesList()) {
			types.add(Property.Type.valueOf(type.name()));
		}

		Property p = new Property(prop.getName(), prop.getValue(), prop.getReadOnly());
		p.setTypes(types);

		return p;
	}

	static TextContainer toTextContainer(net.sf.okapi.proto.TextContainer tc) {
		ArrayList<TextPart> parts = new ArrayList<>();
		for (net.sf.okapi.proto.TextPart part : tc.getPartsList()) {
			parts.add(toTextPart(part));
		}
		// WORKAROUND: Empty TextContainer constructor creates an empty segment.
		// add all the parts as list to avoid this side effect
		TextContainer result = new TextContainer(parts.toArray(new TextPart[0]));
		setINameable(result, tc.getNameable());
		result.setHasBeenSegmentedFlag(tc.getSegApplied());
		
		for (net.sf.okapi.proto.Property property : tc.getNameable().getResource().getPropertiesList()) {
			result.setProperty(toProperty(property));	
		}

		if (tc.getAltTransCount() > 0) {
			AltTranslationsAnnotation ann = new AltTranslationsAnnotation();
			for (net.sf.okapi.proto.AltTranslation at : tc.getAltTransList()) {
				ann.add(toAltTrans(at));
			}
			result.setAnnotation(ann);
		}

		NoteAnnotation na = new NoteAnnotation();
		for (net.sf.okapi.proto.Note note : tc.getNotesList()) {
			na.add(toNote(note));
		}
		result.setAnnotation(na);
		
		return result;
	}

	static Note toNote(net.sf.okapi.proto.Note note) {
		Note result = new Note(note.getNote());
		result.setAnnotates(toAnnotates(note.getAnnotates()));
		result.setPriority(toPriority(note.getPriority()));
		result.setFrom(note.getFrom());
		result.setXmlLang(note.getXmlLang());

		return result;
	}

	static Note.Annotates toAnnotates(net.sf.okapi.proto.Note.Annotates annotates) {
		switch (annotates.name()) {
			case "source": return Note.Annotates.SOURCE;
			case "target": return Note.Annotates.TARGET;
			default: return Note.Annotates.GENERAL;
		}
	}

	static Note.Priority toPriority(net.sf.okapi.proto.Note.Priority priority) {
		switch (priority.getNumber()) {
			case 0: return Note.Priority.ONE;
			case 1: return Note.Priority.TWO;
			case 2: return Note.Priority.THREE;
			case 3: return Note.Priority.FOUR;
			case 4: return Note.Priority.FIVE;
			case 5: return Note.Priority.SIX;
			case 6: return Note.Priority.SEVEN;
			case 7: return Note.Priority.EIGHT;
			case 8: return Note.Priority.NINE;
			default: return Note.Priority.TEN;
		}
	}

	static MatchType toMatchType(net.sf.okapi.proto.AltTranslation.MatchType matchType) {
		switch (matchType.toString()) {
			case "ACCEPTED": return MatchType.ACCEPTED;
			case "EXACT_UNIQUE_ID": return MatchType.EXACT_UNIQUE_ID;
			case "EXACT_LOCAL_CONTEXT": return MatchType.EXACT_LOCAL_CONTEXT;
			case "EXACT": return MatchType.EXACT;
			case "EXACT_TEXT_ONLY": return MatchType.EXACT_TEXT_ONLY;
			case "FUZZY": return MatchType.FUZZY;
			default: return MatchType.UKNOWN;
		}
	}

	static AltTranslation toAltTrans(net.sf.okapi.proto.AltTranslation altTrans) {
		LocaleId srcLoc = LocaleId.fromBCP47(altTrans.getSourceLocale());
		LocaleId trgLoc = LocaleId.fromBCP47(altTrans.getTargetLocale());
		AltTranslation result = new AltTranslation(
				srcLoc,
				trgLoc,
				toTextUnit(altTrans.getTextUnit()),
				toMatchType(altTrans.getType()),
				altTrans.getCombinedScore(),
				altTrans.getOrigin());
		result.setEngine(altTrans.getEngine());
		result.setFromOriginal(altTrans.getFromOriginal());
		result.setFuzzyScore(altTrans.getFuzzyScore());
		result.setQualityScore(altTrans.getQualityScore());
		result.setAltTransType(altTrans.getAltTransType());

		return result;
	}

	static TextUnit toTextUnit(net.sf.okapi.proto.TextUnit value) {
		TextUnit result = new TextUnit("");

		setINameable(result, value.getNameable());
	    setIReferenceable(result, value.getReferenceable());

		result.setSource(toTextContainer(value.getSource()));

		for (Entry<String, net.sf.okapi.proto.TextContainer> trg : value.getTargetsMap().entrySet()) {
			result.setTarget(LocaleId.fromBCP47(trg.getKey()), toTextContainer(trg.getValue()));
		}

		NoteAnnotation na = new NoteAnnotation();
		for (net.sf.okapi.proto.Note note : value.getNotesList()) {
			na.add(toNote(note));
		}
		result.setAnnotation(na);

		return result;
	}

	static void setIResource(IResource out, net.sf.okapi.proto.IResource value) {
		out.setId(value.getId());
		for (net.sf.okapi.proto.Property property : value.getPropertiesList()) {
			out.setProperty(toProperty(property));
		}
		if (value.hasGenericAnnotations()) {
			GenericAnnotations.addAnnotations(out, GenericAnnotations.createFromString(value.getGenericAnnotations().getEncodedAsString()));
		}
	}


	static void setINameable(INameable out, net.sf.okapi.proto.INameable value) {
		setIResource(out, value.getResource());
		out.setName(value.getName());
		out.setType(value.getType());
		out.setMimeType(value.getMimeType());
		out.setIsTranslatable(value.getIsTranslatable());
		out.setPreserveWhitespaces(value.getPreserveWhitespaces());
		for (net.sf.okapi.proto.Property property : value.getResource().getPropertiesList()) {
			out.setProperty(toProperty(property));
		}
	}

	static void setIReferenceable(IReferenceable out, net.sf.okapi.proto.IReferenceable value) {
		out.setReferenceCount(value.getRefCount());
	}


	static StartDocument toStartDocument(net.sf.okapi.proto.StartDocument value) {
		StartDocument result = new StartDocument("");
		setINameable(result, value.getNameable());
		result.setLineBreak(value.getLineBreak());
		result.setLocale(LocaleId.fromBCP47(value.getLocale()));
		result.setEncoding(value.getEncoding(), value.getHasUtf8Bom());
		result.setMultilingual(value.getMultilingual());
		if (result.getName().isEmpty()) {
			result.setName("unknown");
		}
		return result;
	}
	
	static StartGroup toStartGroup(net.sf.okapi.proto.StartGroup value) {
		StartGroup result = new StartGroup();
		result.setParentId(value.getParentId());
		setINameable(result, value.getNameable());
		setIReferenceable(result, value.getReferenceable());
		return result;
	}

	static StartSubfilter toStartSubfilter(net.sf.okapi.proto.StartSubfilter value) {
		StartSubfilter result = new StartSubfilter("", toStartDocument(value.getStartDoc()), null);
		setINameable(result, value.getNameable());
		return result;
	}

	static EndSubfilter toEndSubfilter(net.sf.okapi.proto.EndSubfilter value) {
		EndSubfilter result = new EndSubfilter();
		setIResource(result, value.getResource());
		return result;
	}

	static Ending toEndDocument(net.sf.okapi.proto.EndDocument value) {
		Ending result = new Ending();
		setIResource(result, value.getResource());
		return result;
	}

	static Ending toEndGroup(net.sf.okapi.proto.EndGroup value) {
		Ending result = new Ending();
		setIResource(result, value.getResource());
		return result;
	}


	static Ending toEndSubDocument(EndSubDocument value) {
		Ending result = new Ending();
		setIResource(result, value.getResource());
		return result;
	}

	static StartSubDocument toStartSubDocument(net.sf.okapi.proto.StartSubDocument value) {
		StartSubDocument result = new StartSubDocument();
		result.setParentId(value.getParentId());
		setINameable(result, value.getNameable());
		return result;
	}

	public static Event toEvent(net.sf.okapi.proto.Event event) {
		Event result = Event.createNoopEvent();

		switch (event.getResourceCase()) {
			case ENDDOCUMENT:
				Ending endDocument = toEndDocument(event.getEndDocument());
				result.setResource(endDocument);
				result.setEventType(EventType.END_DOCUMENT);
				break;
			case STARTDOCUMENT:
				StartDocument doc = toStartDocument(event.getStartDocument());
				result.setResource(doc);
				result.setEventType(EventType.START_DOCUMENT);
				break;
			case TEXTUNIT:
				TextUnit tu = toTextUnit(event.getTextUnit());
				result.setResource(tu);
				result.setEventType(EventType.TEXT_UNIT);
				break;
			case STARTGROUP:
				StartGroup startGroup = toStartGroup(event.getStartGroup());
				result.setResource(startGroup);
				result.setEventType(EventType.START_GROUP);
				break;
			case ENDGROUP:
				Ending endGroup = toEndGroup(event.getEndGroup());
				result.setResource(endGroup);
				result.setEventType(EventType.END_GROUP);
				break;
			case STARTSUBFILTER:
				StartSubfilter startSubfilter = toStartSubfilter(event.getStartSubfilter());
				result.setResource(startSubfilter);
				result.setEventType(EventType.START_SUBFILTER);
				break;
			case ENDSUBFILTER:
				EndSubfilter endSubfilter = toEndSubfilter(event.getEndSubfilter());
				result.setResource(endSubfilter);
				result.setEventType(EventType.END_SUBFILTER);
				break;
			case STARTSUBDOCUMENT:
				StartSubDocument startSubDocument = toStartSubDocument(event.getStartSubDocument());
				result.setResource(startSubDocument);
				result.setEventType(EventType.START_SUBDOCUMENT);
				break;
			case ENDSUBDOCUMENT:
				Ending endSubDocument = toEndSubDocument(event.getEndSubDocument());
				result.setResource(endSubDocument);
				result.setEventType(EventType.END_SUBDOCUMENT);
				break;
			default:
				break;
		}

		return result;
	}

	public static List<Event> toEvents(Events value) {
		ArrayList<Event> events = new ArrayList<>();
		for(net.sf.okapi.proto.Event event : value.getEventsList()) {
			events.add(toEvent(event));
		}
		return events;
	}
}

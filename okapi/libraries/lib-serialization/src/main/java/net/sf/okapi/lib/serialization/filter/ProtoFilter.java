/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.serialization.filter;

import com.google.protobuf.util.JsonFormat;
import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;
import net.sf.okapi.common.filters.AbstractFilter;
import net.sf.okapi.common.filters.EventBuilder;
import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.lib.serialization.Proto2Okapi;
import net.sf.okapi.lib.serialization.writer.ProtoBufferWriter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * An {@link IFilter} that returns serialized {@link Event}s generated by
 * {@link ProtoBufferWriter}
 * 
 * @author jimh
 *
 */
public class ProtoFilter extends AbstractFilter {
	private static final String MIMETYPE = "text/x-protobuf";
	private RawDocument input;	
	private EventBuilder eventBuilder;
	private boolean binary;
	private Parameters params;
	private InputStream inputStream;
	private net.sf.okapi.proto.Event.Builder builder;

	public ProtoFilter() {
		this.params = new Parameters();
		setMimeType(MIMETYPE);
		setMultilingual(true);
		setName("okf_protobuf");
		setDisplayName("Protobuf Filter");
		addConfiguration(new FilterConfiguration(getName(), MIMETYPE, getClass().getName(),
				"Protobuf", "Configuration for Protobuf files of various types", null, ".json;"));
	}

	@Override
	public void open(RawDocument input) {
		open(input, false);
	}

	@Override
	public void open(RawDocument input, boolean generateSkeleton) {
		binary = false;
		this.input = input;
		this.input.setEncoding(StandardCharsets.UTF_8);

		if (params.getOutput() == Parameters.PROTOBUF_OUTPUT.BINARY) {
			binary = true;
			inputStream = this.input.getStream();
		}

		// create EventBuilder with document name as rootId
		if (eventBuilder == null) {
			eventBuilder = new EventBuilder(getParentId(), this);
		} else {
			eventBuilder.reset(getParentId(), this);
		}
		eventBuilder.setMimeType(MIMETYPE);
		eventBuilder.setPreserveWhitespace(true);

		builder = net.sf.okapi.proto.Event.newBuilder();
	}

	@Override
	public boolean hasNext() {
		return eventBuilder.hasNext();
	}

	@Override
	public Event next() {
		if (eventBuilder.hasQueuedEvents()) {
			return eventBuilder.next();
		}

		if (params.getOutput() == Parameters.PROTOBUF_OUTPUT.BINARY) {
			Event e = Proto2Okapi.toEvent(loadFromBinary());
			eventBuilder.addFilterEvent(e);
		} else if (params.getOutput() == Parameters.PROTOBUF_OUTPUT.JSON_LINES) {
			try {
				try (BufferedReader reader = new BufferedReader(this.input.getReader())) {
					String line;
					while ((line = reader.readLine()) != null && !line.isEmpty()) {
						Event e = Proto2Okapi.toEvent(loadFromJson(line));
						eventBuilder.addFilterEvent(e);
						builder.clear();
					}
				}
			} catch (IOException e) {
				throw new OkapiBadFilterInputException("Error parsing or converting JSON Event", e);
			}
		}

		// return one of the waiting events
		return eventBuilder.next();
	}

	/**
	 * Deserializes a single Event from JSON
	 *
     */
	protected net.sf.okapi.proto.Event loadFromJson(String json) throws IOException {
		JsonFormat.parser().merge(json, builder);
		return builder.build();
	}

	/**
	 * Deserializes a single Event from binary
     */
	protected net.sf.okapi.proto.Event loadFromBinary() {
		try {
			return net.sf.okapi.proto.Event.parseDelimitedFrom(inputStream);
		} catch (IOException | UnsupportedOperationException e) {
			throw new OkapiBadFilterInputException("Error parsing or converting Protobuf Events", e);
		}
	}

	@Override
	public Parameters getParameters() {
		return params;
	}

	@Override
	public void setParameters(IParameters params) {
		this.params = (Parameters) params;
	}

	@Override
	protected boolean isUtf8Encoding() {
		return !binary;
	}

	@Override
	public void close() {
		super.close();
		if (input != null) {
			input.close();
		}
	}
}

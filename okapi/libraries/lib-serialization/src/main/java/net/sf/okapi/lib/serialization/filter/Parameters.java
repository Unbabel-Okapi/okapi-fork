/*
 * ====================================================================
 *   Copyright (C) $time.year by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =====================================================================
 */

package net.sf.okapi.lib.serialization.filter;

import net.sf.okapi.common.StringParameters;

public class Parameters extends StringParameters {
	private static final String OUTPUT = "output";
	private static final String COPY_SOURCE = "copySource";

	public enum PROTOBUF_OUTPUT {
		JSON_LINES,
		BINARY
	}

	public Parameters() {
		super();
	}

	@Override
	public void reset() {
		super.reset();
		setOutput(PROTOBUF_OUTPUT.JSON_LINES);
		setCopySource(false);
	}

	public void setOutput(PROTOBUF_OUTPUT output) {
		setString(OUTPUT, output.name());
	}
	
	public PROTOBUF_OUTPUT getOutput() {
		return PROTOBUF_OUTPUT.valueOf(getString(OUTPUT));
	}

	public void setCopySource(boolean copySource) {
		setBoolean(COPY_SOURCE, copySource);
	}

	public boolean isCopySource() {
		return getBoolean(COPY_SOURCE);
	}
}

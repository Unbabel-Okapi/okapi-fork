/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.serialization.writer;

import com.google.protobuf.util.JsonFormat;
import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.exceptions.OkapiIOException;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.skeleton.ISkeletonWriter;
import net.sf.okapi.lib.serialization.Okapi2Proto;
import net.sf.okapi.lib.serialization.filter.Parameters;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class ProtoBufferWriter implements IFilterWriter {
	private OutputStream outputStream;
	private String outputPath;
	private LocaleId sourceLocale;
	private LocaleId targetLocale;
	private boolean canceled;
	private Parameters params;
	private OutputStreamWriter outputWriter;

    public ProtoBufferWriter() {
		this.params = new Parameters();
		this.params.setOutput(Parameters.PROTOBUF_OUTPUT.JSON_LINES);
	}
	
	@Override
	public String getName() {
		return getClass().getName();
	}

	@Override
	public void setOptions(LocaleId targetLocale, String defaultEncoding) {
		this.targetLocale = targetLocale;
		// defaultEncoding is ignored: we always use UTF-8
	}

	@Override
	public void setOutput(String path) {
		this.outputPath = path;
	}

	@Override
	public void setOutput(OutputStream output) {
		// If we use the stream, we can't use the path
		this.outputPath = null;
		this.outputStream = output;
	}

	@Override
	public Event handleEvent(Event event) {
		if (canceled) {
			return new Event(EventType.CANCELED);
		}
		try {
			switch (event.getEventType()) {
				case START_DOCUMENT:
					processStartDocument(event);
					break;
				case TEXT_UNIT:
					processTextUnit(event);
					break;
				case START_GROUP:
				case START_SUBFILTER:
				case START_SUBDOCUMENT:
				case END_GROUP:
				case END_SUBFILTER:
				case END_SUBDOCUMENT:
				case END_DOCUMENT:
					break;
				default:
					event = Event.createNoopEvent();
					break;
			}

			saveEvent(event);
		} catch (IOException e) {
			throw new OkapiIOException(e);
		}
		return event;
	}

	private void processTextUnit(Event event) throws IOException {
		if (params.isCopySource() && (!event.getTextUnit().hasTarget(targetLocale) || event.getTextUnit().getTarget(targetLocale).isEmpty())) {
			TextContainer tc = event.getTextUnit().getSource().clone();
			event.getTextUnit().setTarget(targetLocale, tc);
		}
	}

	private void saveEvent(Event event) throws IOException {
		switch (params.getOutput()) {
			case BINARY:
				saveToBinaryFile(Okapi2Proto.toEvent(event, sourceLocale));
				break;
			case JSON_LINES:
				saveToJsonFile(Okapi2Proto.toEvent(event, sourceLocale));
				break;
		}
	}

	private void saveToJsonFile(net.sf.okapi.proto.Event proto) throws IOException {
		outputWriter.write(JsonFormat.printer().includingDefaultValueFields().omittingInsignificantWhitespace().print(proto));
		outputWriter.write("\n");
	}

	private void saveToBinaryFile(net.sf.okapi.proto.Event proto) throws IOException {
		proto.writeDelimitedTo(outputStream);
	}
	
	private void processStartDocument(Event event) throws IOException {
		if ( outputStream == null ) {	
			try {
				outputStream = new FileOutputStream(outputPath);
			} catch (FileNotFoundException e) {
				throw new OkapiIOException("Error serializing TextUnit", e);
			}
		}

		if (params.getOutput() != Parameters.PROTOBUF_OUTPUT.BINARY) {
			outputWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
		}
		sourceLocale = event.getStartDocument().getLocale();
	}

	@Override
	public void close() {
		if (outputWriter != null) {
			try {
				outputWriter.close();
				outputWriter = null;
			} catch (IOException e) {
				throw new OkapiIOException("Error closing JSON writer", e);
			}
		}
		
		if (outputStream != null) {
			try {
				outputStream.close();
				outputStream = null;
			} catch (IOException e) {
				throw new OkapiIOException("Error closing protobuf stream", e);
			}
		}
	}

	@Override
	public Parameters getParameters() {
		return params;
	}

	@Override
	public void setParameters(IParameters params) {
		this.params = (Parameters)params;		
	}

	@Override
	public void cancel() {
		close();
		this.canceled = true;
	}

	@Override
	public EncoderManager getEncoderManager() {
		return null;
	}

	@Override
	public ISkeletonWriter getSkeletonWriter() {
		return null;
	}
}

syntax = "proto3";

option java_package = "net.sf.okapi.proto";
option java_outer_classname = "OkapiProtoMapping";
option java_multiple_files = true;

message Events {
  repeated Event events = 1;
}

message Event {
  oneof resource {
    TextUnit textUnit = 1;

    StartDocument startDocument = 2;
    EndDocument endDocument = 3;

    StartGroup startGroup = 4;
    EndGroup endGroup = 5;

    StartSubfilter startSubfilter = 6;
    EndSubfilter endSubfilter = 7;

    StartSubDocument startSubDocument = 8;
    EndSubDocument endSubDocument = 9;
  }
}

message IResource {
  string   id = 1;
  repeated Property properties = 2;
  GenericAnnotations genericAnnotations = 3;
}

message INameable {
  IResource resource = 1;
  string name = 2;
  string type = 3;
  string mimeType = 4;
  bool isTranslatable = 5;
  bool preserveWhitespaces = 6;
}

message IReferenceable {
  int32 refCount = 1;
}

message StartDocument {
  INameable nameable = 1;
  string    lineBreak = 2;
  string    locale = 3;
  string    encoding = 4;
  bool      multilingual = 5;
  bool      hasUtf8Bom = 6;
  string    parameters = 7;
}

message EndDocument {
  IResource resource = 1;
}

message StartSubDocument {
  INameable nameable = 1;
  string parentId = 2;
  string filterId = 3;
}

message EndSubDocument {
  IResource resource = 1;
}

message StartGroup {
  string parentId = 1;
  INameable nameable = 2;
  IReferenceable referenceable = 3;
}

message EndGroup {
  IResource resource = 1;
}

message StartSubfilter {
  INameable nameable = 1;
  StartDocument startDoc = 2;
}

message EndSubfilter {
  IResource resource = 1;
}

message Property {
  enum PropName {
    ENCODING = 0;
    LANGUAGE = 1;
    APPROVED = 2;
    NOTE = 3;
    TRANSNOTE = 4;
    COORDINATES = 5;
    STATE_QUALIFIER = 6;
    STATE = 7;
    ITS_LQI = 8;
    ITS_PROV = 9;
    ITS_MTCONFIDENCE = 10;
    XLIFF_TOOL = 11;
    XLIFF_PHASE = 12;
    MAX_WIDTH = 13;
    MAX_HEIGHT = 14;
    SIZE_UNIT = 15;
    /**
     * For TMX filter - may be used by {@link TMXWriter} or {@link TMXFilterWriter}
     */
    TMX_i = 16;
    TMX_hi = 17;
    TMX_x = 18;
  }

  enum PropertyType {
    FILTER_ONLY = 0;
    DISPLAY = 1;
    ITS = 2;
    PIPELINE = 3;
  }

  string name = 1;
  optional string value = 2;
  optional bool readOnly = 3;
  repeated PropertyType types = 4;
}

message Note {
  enum Priority {
    ONE = 0;
    TWO = 1;
    THREE = 2;
    FOUR = 3;
    FIVE = 4;
    SIX = 5;
    SEVEN = 6;
    EIGHT = 7;
    NINE = 8;
    TEN = 9;
  }

  enum Annotates {
    SOURCE = 0;
    TARGET = 1;
    GENERAL = 2;
  }

  // Required
  string note = 1;

  // optional
  optional string xmlLang = 2;
  optional string from = 3;
  optional Priority priority = 4;
  optional Annotates annotates = 5;
}

message PrimitiveValue {
  oneof value_type {
    string string_value = 1;
    bool bool_value = 2;
    int32 int_value = 3;
    double double_value = 4;
  }
}

message GenericAnnotation {
  string type = 1;
  map<string, PrimitiveValue> fields = 2;
}

message GenericAnnotations {
  string encodedAsString = 1;
  repeated GenericAnnotation genericAnnotations = 2;
}

message TextUnit {
  INameable nameable = 1;
  IReferenceable referenceable = 2;
  string parentId = 3;
  TextContainer source = 4;
  map<string, TextContainer> targets = 5;
  repeated Note notes = 6;
}

/**
 should only appear on the target TextContainer or target TextFragment (segment)
 denotes an alternate translation for the current TextUnit
 as found in xliff 1.2 alt-trans element
**/
message AltTranslation {
  enum MatchType {
    ACCEPTED = 0;
    HUMAN_RECOMMENDED = 1;
    EXACT_UNIQUE_ID = 2;
    EXACT_PREVIOUS_VERSION = 3;
    EXACT_LOCAL_CONTEXT = 4;
    EXACT_DOCUMENT_CONTEXT = 5;
    EXACT_STRUCTURAL = 6;
    EXACT = 7;
    EXACT_TEXT_ONLY_UNIQUE_ID = 8;
    EXACT_TEXT_ONLY_PREVIOUS_VERSION = 9;
    EXACT_TEXT_ONLY = 10;
    EXACT_REPAIRED = 11;
    FUZZY_UNIQUE_ID = 12;
    FUZZY_PREVIOUS_VERSION = 13;
    FUZZY = 14;
    FUZZY_REPAIRED = 15;
    PHRASE_ASSEMBLED = 16;
    MT = 17;
    CONCORDANCE = 18;
    UKNOWN = 19;
  }

  string sourceLocale = 1;
  string targetLocale = 2;
  TextUnit textUnit = 3;
  int32 combinedScore = 4;
  int32 fuzzyScore = 5;
  optional int32 qualityScore = 6;
  optional string origin = 7;
  bool fromOriginal = 8;
  optional string engine = 9;
  string altTransType = 10;
  optional string currentToolId = 11;
  MatchType type = 12;
}

message TextContainer {
  INameable nameable = 1;
  string locale = 2;
  repeated TextPart parts = 3;
  bool segApplied = 4;
  repeated AltTranslation altTrans = 5;
  repeated Note notes = 6;
}

message TextPart {
  enum WhitespaceStrategy {
    INHERIT = 0;
    PRESERVE = 1;
    NORMALIZE = 2;
  }

  IResource resource = 1;
  optional string originalId = 2;
  TextFragment text = 3;
  WhitespaceStrategy whitespaceStrategy = 4;
  bool segment = 6;
}

message TextFragment {
  enum TagType {
    OPENING = 0;
    CLOSING = 1;
    PLACEHOLDER = 2;
  }
  string text = 1;
  repeated Code codes = 2;
  bool balanced = 3;
}

message Code {
  int32 id = 1;
  TextFragment.TagType tagType = 2;
  optional string originalId = 3;
  string codeType = 4;
  string data = 5;
  optional string outerData = 6;
  int32 flag = 7;
  optional string mergedData = 8;
  int32 position = 9;
  bool isolated = 10;
  bool added = 11;
  optional string displayText = 12;
  repeated Property properties = 13;
  map<string, string> inlineAnnotations = 14;
}

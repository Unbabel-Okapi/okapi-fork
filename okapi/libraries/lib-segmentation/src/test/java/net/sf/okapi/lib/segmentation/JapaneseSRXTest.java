/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.segmentation;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class JapaneseSRXTest {
	private static OkapiSegmenter segmenter;

	@BeforeClass
	public static void init() {
		segmenter = new OkapiSegmenter("ja");
	}

	@Test
	public void okapiSegmentTest() {
		segment();
	}

	private void segment() {
		test("これはテスト用の文です。");
		test("これはテスト用の文です。", "追加のテスト用の文です。");
		test("これは、テスト用の文です。");
		test("テスト用の文です！",  "追加のテスト用の文です。");
		test("テスト用の文です...",  " 追加のテスト用の文です。");
		test("アドレスはhttp://www.test.deです。");

		test("これは(!)の文です。");
		test("これは(!!)の文です。");
		test("これは(?)の文です。");
		test("これは(??)の文です。");
		test("自民党税制調査会の幹部は、「引き下げ幅は３．２９％以上を目指すことになる」と指摘していて、今後、公明党と合意したうえで、３０日に決定する与党税制改正大綱に盛り込むことにしています。\", \"２％台後半を目指すとする方向で最終調整に入りました。");
		test("フフーの主たる債務");
	}

	private void test(String... sentences) {
		SrxSplitCompare.compare(sentences, segmenter);
	}
}

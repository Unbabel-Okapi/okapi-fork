/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.lib.segmentation;

import junit.framework.Assert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class SrxSplitCompare {

	public static void compare(final String[] sentences, final OkapiSegmenter sTokenizer) {
		final StringBuilder inputString = new StringBuilder();
		final List<String> input = new ArrayList<>();
		Collections.addAll(input, sentences);
		for (final String s : input) {
			inputString.append(s);
		}
		List<String> segments = sTokenizer.tokenize(inputString.toString());
		Assert.assertEquals(input, segments);
	}
}

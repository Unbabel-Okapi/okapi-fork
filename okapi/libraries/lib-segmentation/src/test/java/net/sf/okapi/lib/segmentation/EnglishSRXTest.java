/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.lib.segmentation;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class EnglishSRXTest {
  private static OkapiSegmenter segmenter;

  @BeforeClass
  public static void init() {
    segmenter = new OkapiSegmenter("en");
  }

  @Test
  public void okapiSegmentTest() {
	  segment();
  }

  @Test
  public void okapiSegmentTest2() {
    test("1. The first item.");
  }

  // NOTE: sentences here need to end with a space character so they
  // have correct whitespace when appended:
  private void segment() {
    // incomplete sentences, need to work for on-thy-fly checking of texts:
	test("Ne. 10:17–19");
    test("Here's a");
    test("Here's a sentence.", " And here's one that's not comp");

    test("This is a sentence. ");
    test("This is a sentence.", " And this is another one.");
    test("This is a sentence.", " Isn't it?", " Yes, it is.");
    test("This is e.g. Mr. Smith, who talks slowly...", "But this is another sentence.");
    test("Chanel no. 5 is blah.");
    test("Mrs. Jones gave Peter $4.5, to buy Chanel No 5.", " He never came back.");
    test("On p. 6 there's nothing.", " Another sentence.");
    test("Leave me alone!, he yelled.", " Another sentence.");
    test("\"Leave me alone!\", he yelled.");
    test("'Leave me alone!', he yelled.", " Another sentence.");
    test("'Leave me alone!,' he yelled.", " Another sentence.");
    test("This works on the phrase level, i.e. not on the word level.");
    test("Let's meet at 5 p.m. in the main street.");
    test("James comes from the U.K. where he worked as a programmer.");
    test("Don't split strings like U.S.A. please.");
    test("Don't split strings like U. S. A. either.");
    test("Don't split...", "Well you know.", " Here comes more text.");
    test("Don't split... well you know.", " Here comes more text.");
    test("The \".\" should not be a delimiter in quotes.");
    test("\"Here he comes!\", she said.");
    test("\"Here he comes.\"", " But this is another sentence.");
    test("\"Here he comes!\".", " That's what he said.");
    test("The sentence ends here.", " (Another sentence.)");
    test("The sentence (...) ends here.");
    test("The sentence [...] ends here.");
    test("The sentence ends here (...).", " Another sentence.");
    // previously known failed but not now :)
    test("He won't.", " Really.");
    test("He will not.", " Really.");
    test("He won't go.", " Really.");
    test("He won't say no. 5 is better.", " Not really.");
    test("He won't say No. 5 is better.", " Not really.");
    test("He won't say no.", " Not really.");
    test("He won't say No.", " Not really.");
    test("They met at 5 p.m. on Thursday.");
    test("They met at 5 p.m.", " It was Thursday.");
    test("This is it: a test.");
    test("12) Make sure that the lamp is on.", " 12) Make sure that the lamp is on. ");
    test("He also offers a conversion table (see Cohen, 1988, p. 123). ");
    // one/two returns = paragraph = new sentence:
    test("He won't", "\n\n", "Really.");
    test("He won't", "\n", "Really.");
    test("He won't", "\n", "Really.");
    // Missing space after sentence end:
    test("James is from the Ireland!", " He lives in Spain now.");
    // From the abbreviation list:
    test("Jones Bros. have built a successful company.");
    // parentheses:
    test("It (really!) works.");
    test("It [really!] works.");
    test("It works (really!).", " No doubt.");
    test("It works [really!].", " No doubt.");
    test("It really(!) works well.");
    test("It really[!] works well.");
    test("Translation:", " Rock-bottom prices for local growers.", " \"You're going to lose farmers to bankruptcy; there's no way else to say it,\" says Turp Garrett, the agricultural extension agent for Worcester County, Md.\"", " It's not very pretty.\"");
    test("My name is Jonas E. Smith.");
    test("Please turn to p. 55.");
    test("Were Jane and co. at the party?");
    test("They closed the deal with Pitt, Briggs & Co. at noon.");
    test("Let's ask Jane and co.", " They should know.");
    test("They closed the deal with Pitt, Briggs & Co.", " It closed yesterday.");
    test("I can see Mt. Fuji from here.");
    test("St. Michael's Church is on 5th st. near the light.");
    test("That is JFK Jr.'s book.");
    test("I visited the U.S.A. last year.");
    test("I live in the E.U.", " How about you?");
    test("I live in the U.S.", " How about you?");
    test("I have lived in the U.S. for 20 years.");
    test("At 5 a.m. Mr. Smith went to the bank.", " He left the bank at 6 P.M.",  " They then went to the store.");
    test("She has $100.00.", " It is in her bag.");
    test("He teaches science (He previously worked for 5 years as an engineer.) at the local University.");
    test("Her email is Jane.Doe@example.com.", " I sent her an email.");
    test("The site is: https://www.example.50.com/new-site/awesome_content.html.", " Please check it out.");
    test("She turned to him, 'This is great.' she said.");
    test("She turned to him, \"This is great.\" she said.");
    test("She turned to him, \"This is great.\"", " She held the book out to show him.");
    test("Hello!!", " Long time no see.");
    test("Hello??", " Who is there?");
    test("Hello!?", " Is that you?");
    test("Hello?!", " Is that you?");
    test("You can find it at N°. 1026.253.553.", " That is where the treasure is.");
    test("She works at Yahoo! in the accounting department.");
    test("The nurse gave him the i.v. in his vein.", " She gave him the i.v.", " It was a great I.V. that she gave him.", " She gave him the I.V.", " It was night.");
    test("Leave me alone!", " He yelled.", " I am in the U.S. Army.", " Charles (Ind.) said he.");
    
    test("The GmbH & Co. KG is a limited partnership with, typically, the sole general partner being a limited liability company.");
    test("\"It's a good thing that the water is really calm,\" I answered ironically.");
    test("December 31, 1988.", " Hello world.", " It's great!", " Born April 05, 1989.");
    test("[A sentence in square brackets.]");
    test("This abbreviation f.e. means for example.");
    test("The med. staff here is very kind.");
    test("What did you order btw., she wondered.");
    test("This is the U.S. Senate my friends.", " Yes.", " It is!");
    test("SEC. 1262 AUTHORIZATION OF APPROPRIATIONS.");
    test("Hello.", " 'This is a test of single quotes.'", " A new sentence.");

    test("1.) The first item.", " 2.) The second item.");
    test("1) The first item.", " 2) The second item.");
    
    test("1.) The first item 2.) The second item");
    test("1) The first item 2) The second item");
    test("1. The first item.");
    test("a. The first item.");
    test("https://www.example.com/foo/?bar=baz&inga=42&quux");
    test("[A sentence in square brackets.]");
    test("Hello.", " 'This is a test of single quotes.'", " A new sentence.");
    test("\"Dinah'll miss me very much to-night, I should think!\"", " (Dinah was the cat.)");
    test("The nurse gave him the i.v. in his vein.", " She gave him the i.v.", " It was a great I.V. that she gave him.", " She gave him the I.V.", " It was night.");
    test("On Jan. 20, former Sen. Barack Obama became the 44th President of the U.S.", " Millions attended the Inauguration.");
    test("He works for ABC Ltd. and sometimes for BCD Ltd.", " She works for ABC Co. and BCD Co.", " They work for ABC Corp. and BCD Corp.");
    test("Rolls-Royce Motor Cars Inc. said it expects its U.S. sales to remain steady at about 1,200 cars in 1990.", " 'So what if you miss 50 tanks somewhere?'", " asks Rep. Norman Dicks (D. Wash.), a member of the House group that visited the talks in Vienna.");
  }
  
  private void test(String... sentences) {
	  SrxSplitCompare.compare(sentences, segmenter);
  }
}

/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.lib.segmentation;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class DanishSRXTest {
	private static OkapiSegmenter segmenter;

	@BeforeClass
	public static void init() {
		segmenter = new OkapiSegmenter("da");
	}

	@Test
	public void testOkapiSegmentTest() {
		segment();
	}

	private void segment() {
		// NOTE: sentences here need to end with a space character so they
		// have correct whitespace when appended:
		test("Dette er en sætning.");
		test("Dette er en sætning.", " Her er den næste.");
		test("En sætning!", " Yderlige en.");
		test("En sætning...", "Yderlige en.");
		test("På hjemmesiden http://www.stavekontrolden.dk bygger vi stavekontrollen.");
		test("Den 31.12. går ikke!");
		test("Den 3.12.2011 går ikke!");
		test(
				"I det 18. og tidlige 19. århundrede hentede amerikansk kunst det meste af sin inspiration fra Europa.");

		test(
				"Hendes Majestæt Dronning Margrethe II (Margrethe Alexandrine Þórhildur Ingrid, Danmarks dronning) (født 16. april 1940 på Amalienborg Slot) er siden 14. januar 1972 Danmarks regent.");
		test("Hun har residensbolig i Christian IX's Palæ på Amalienborg Slot.");
		test("Tronfølgeren ledte herefter statsrådsmøderne under Kong Frederik 9.'s fravær.");
		test("Marie Hvidt, Frederik IV - En letsindig alvorsmand, Gads Forlag, 2004.");
		// FIXME: testSplit("Da vi første gang besøgte Restaurant Chr. IV, var vi de eneste gæster.");

		test("I dag er det den 25.12.2010.");
		// FIXME: testSplit("I dag er det d. 25.12.2010.");
		test("I dag er den 13. december.");
		test("Arrangementet starter ca. 17:30 i dag.");
		test("Arrangementet starter ca. 17:30.");
		test("Det er nævnt i punkt 3.6.4 Rygbelastende helkropsvibrationer.");

		test(
				"Rent praktisk er det også lettest lige at mødes, så der kan udveksles nøgler og brugsanvisninger etc.");
		test(
				"Andre partier incl. borgerlige partier har deres særlige problemer: nogle samarbejder med apartheidstyret i Sydafrika, med NATO-landet Tyrkiet etc., men det skal så sandelig ikke begrunde en SF-offensiv for et samarbejde med et parti.");

		test("Hvad nu,, den bliver også.");
		test("Det her er det..", " Og her fortsætter det.");

		test("Dette er en(!) sætning.");
		test("Dette er en(!!) sætning.");
		test("Dette er en(?) sætning.");
		test("Dette er en(??) sætning.");
		test("Dette er en(???) sætning.");
		test("Militær værnepligt blev indført (traktaten krævede, at den tyske hær ikke oversteg 100.000 mand).");

		test(
				"Siden illustrerede hun \"Historierne om Regnar Lodbrog\" 1979 og \"Bjarkemål\" 1982 samt Poul Ørums \"Komedie i Florens\" 1990.");
	}

	private void test(String... sentences) {
		SrxSplitCompare.compare(sentences, segmenter);
	}
}

/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.lib.segmentation;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class GermanSRXTest {
	private static OkapiSegmenter segmenter;

	@BeforeClass
	public static void init() {
		segmenter = new OkapiSegmenter("de");
	}

	@Test
	public void testOkapiSegmentTest() {
		segment();
	}

	private void segment() {
		test("Dies ist ein Satz.");
		test("Dies ist ein Satz.", " Noch einer.");
		//testSplit("Dies ist ein Satz.¹", " Noch einer.");
		test("Ein Satz!", " Noch einer.");
		test("Ein Satz...", "Noch einer.");
		test("Unter http://www.test.de gibt es eine Website.");
		test("Das Schreiben ist auf den 3.10. datiert.");
		test("Das Schreiben ist auf den 31.1. datiert.");
		test("Das Schreiben ist auf den 3.10.2000 datiert.");
		test("Natürliche Vererbungsprozesse prägten sich erst im 18. und frühen 19. Jahrhundert aus.");
		test("Das ist ja 1a. und das auch.");

		test("Friedrich I., auch bekannt als Friedrich der Große.");
		test("Friedrich II., auch bekannt als Friedrich der Große.");
		test("Friedrich IIXC., auch bekannt als Friedrich der Große.");
		test("Friedrich II. öfter auch bekannt als Friedrich der Große.");
		test("Friedrich VII. öfter auch bekannt als Friedrich der Große.");
		test("Friedrich X. öfter auch bekannt als Friedrich der Zehnte.");

		test("Heute ist der 13.12.2004.");
		test("Heute ist der 13. Dezember.");
		test("Heute ist der 1. Januar.");
		test("Es geht am 24.09. los.");
		test("Es geht um ca. 17:00 los.");
		test("Das in Punkt 3.9.1 genannte Verhalten.");

		test("Diese Periode begann im 13. Jahrhundert und damit bla.");
		test("Diese Periode begann im 13. oder 14. Jahrhundert und damit bla.");
		test("Diese Periode datiert auf das 13. bis zum 14. Jahrhundert und damit bla.");

		test("Das gilt lt. aktuellem Plan.");
		test("Orangen, Äpfel etc. werden gekauft.");

		test("Das ist,, also ob es bla.");
		test("Das ist es..", " So geht es weiter.");

		test("Das hier ist ein(!) Satz.");
		test("Das hier ist ein(!!) Satz.");
		test("Das hier ist ein(?) Satz.");
		test("Das hier ist ein(???) Satz.");
		test("Das hier ist ein(???) Satz.");

		test("»Der Papagei ist grün.«", " Das kam so.");
		test("»Der Papagei ist grün«, sagte er");

		// TODO: derzeit unterscheiden wir nicht, ob nach dem Doppelpunkt ein
		// ganzer Satz kommt oder nicht:
		test("Das war es: gar nichts.");
		test("Das war es:", " Dies ist ein neuer Satz.");

		// Tests created as part of regression testing of SRX tokenizer.
		// They come from Schuld und Sühne (Crime and Punishment) book.
		test("schlug er die Richtung nach der K … brücke ein.");
		test("sobald ich es von einem Freunde zurückbekomme …« Er wurde verlegen und schwieg.");
		// testSplit(new String[] { "Verstehen Sie wohl? ", "… ", "Gestatten Sie
		// mir noch die Frage" });
		test("Aus denen er schöpfen konnte d. h. natürlich.");
		test("Er kannte eine Unmenge Quellen, aus denen er schöpfen konnte, d. h. natürlich, wo er durch Arbeit sich etwas verdienen konnte.");
		test("Stimme am lautesten heraustönte ….", " Sobald er auf der Straße war");
		// testSplit(new String[] { "Aber nein doch, er hörte alles nur zu
		// deutlich! ", "\n", "… ", "›Also, wenn's so ist" });
		test("»Welche Wohnung?\"", " »Die, wo wir arbeiten.");
		test("»Nun also, wie ist's?« fragte Lushin und blickte sie fest an.");
	}

	private void test(String... sentences) {
		SrxSplitCompare.compare(sentences, segmenter);
	}
}

package net.sf.okapi.tm.pensieve.tmx;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.filters.tmx.TmxFilter;
import net.sf.okapi.tm.pensieve.common.MetadataType;
import net.sf.okapi.tm.pensieve.common.TmHit;
import net.sf.okapi.tm.pensieve.common.TranslationUnit;
import net.sf.okapi.tm.pensieve.seeker.ITmSeeker;
import net.sf.okapi.tm.pensieve.seeker.PensieveSeeker;
import net.sf.okapi.tm.pensieve.seeker.TmSeekerFactory;
import net.sf.okapi.tm.pensieve.writer.ITmWriter;
import net.sf.okapi.tm.pensieve.writer.PensieveWriter;

import org.apache.lucene.store.Directory;
import org.apache.lucene.store.ByteBuffersDirectory;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TmxHandlerImportIT {

	private final static LocaleId locENUS = LocaleId.fromString("EN-US");
	private final static LocaleId locDEDE = LocaleId.fromString("DE-DE");
	private final static LocaleId locEN = LocaleId.fromString("EN");
	private final static LocaleId locIT = LocaleId.fromString("IT");
	private ITmSeeker seeker;
	private FileLocation root;

	@Before
	public void setUP() {
		root = FileLocation.fromClass(getClass());
		seeker = TmSeekerFactory.createFileBasedTmSeeker(root.in("/").toString());
	}
	
	@After
    public void tearDown() {
		if (seeker != null) {
			seeker.close();
		}
    }

	@Test
	public void importTmx_paragraph_tmx_basics() throws Exception {
		TmxFilter tmxFilter = new TmxFilter();
		Directory ramDir = new ByteBuffersDirectory();
		ITmWriter tmWriter = new PensieveWriter(ramDir, true);
		OkapiTmxImporter tmxHandler = new OkapiTmxImporter(locENUS, tmxFilter);
		tmxHandler.importTmx(root.in("/Paragraph_TM.tmx").asUri(), locDEDE, tmWriter);
		tmWriter.close();

		try (ITmSeeker seeker = new PensieveSeeker(ramDir)) {
			TranslationUnit tu = seeker
					.searchExact(
							new TextFragment(
									"Pumps have been paused for 3 minutes. Consider starting a saline drip."),
							null).get(0).getTu();
			assertEquals(
					"tu target content",
					"Pumpen wurden 3 Minuten lang angehalten, ggf. NaCl-Infusion starten",
					tu.getTarget().getContent().toText());
		}
	}

	@Test
	public void importTmx_sample_tmx_basics() throws Exception {
		TmxFilter tmxFilter = new TmxFilter();
		Directory ramDir = new ByteBuffersDirectory();
		ITmWriter tmWriter = new PensieveWriter(ramDir, true);
		OkapiTmxImporter tmxHandler = new OkapiTmxImporter(locEN, tmxFilter);
		tmxHandler.importTmx(root.in("/sample_tmx.xml").asUri(), locIT, tmWriter);
		tmWriter.close();

		try (ITmSeeker seeker = new PensieveSeeker(ramDir)) {
			TranslationUnit tu = seeker
					.searchExact(new TextFragment("hello"), null).get(0).getTu();
			assertEquals("tu target content", "ciao", tu.getTarget().getContent()
					.toText());
			assertEquals("tu source content", "hello", tu.getSource().getContent()
					.toText());
			tu = seeker.searchExact(new TextFragment("world"), null).get(0).getTu();
			assertEquals("tu target content", "mondo", tu.getTarget().getContent()
					.toText());
			assertEquals("tu source content", "world", tu.getSource().getContent()
					.toText());
		}
	}

	@Test
	public void importTmx_sample_metadata() throws Exception {
		TmxFilter tmxFilter = new TmxFilter();
		Directory ramDir = new ByteBuffersDirectory();
		ITmWriter tmWriter = new PensieveWriter(ramDir, true);
		OkapiTmxImporter tmxHandler = new OkapiTmxImporter(locEN, tmxFilter);
		tmxHandler.importTmx(root.in("/sample_tmx.xml").asUri(), locIT, tmWriter);
		tmWriter.close();

		try (ITmSeeker seeker = new PensieveSeeker(ramDir)) {
			TranslationUnit tu = seeker
					.searchExact(new TextFragment("hello"), null).get(0).getTu();
			assertEquals("# of metadata (not ignored)", 4, tu.getMetadata().size());
			assertEquals("tu id", "hello123", tu.getMetadata().get(MetadataType.ID));
			assertEquals("tu FileName", "GeorgeInTheJungle.hdf", tu.getMetadata()
					.get(MetadataType.FILE_NAME));
			assertEquals("tu GroupName", "ImAGroupie", tu.getMetadata().get(
					MetadataType.GROUP_NAME));
			assertEquals("tu Type", "plaintext", tu.getMetadata().get(
					MetadataType.TYPE));
		}
	}

}

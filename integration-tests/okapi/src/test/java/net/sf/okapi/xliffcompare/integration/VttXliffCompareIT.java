package net.sf.okapi.xliffcompare.integration;

import net.sf.okapi.common.integration.XliffCompareIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.vtt.VTTFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class VttXliffCompareIT extends XliffCompareIT {
	private static final String CONFIG_ID = "okf_vtt";
	private static final String DIR_NAME = "/vtt/";
	private static final List<String> EXTENSIONS = Arrays.asList(".vtt", ".srt");

	public VttXliffCompareIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, VTTFilter::new);
	}

	@Test
	public void vttXliffCompareFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(false, new FileComparator.XmlComparator());
	}
}
